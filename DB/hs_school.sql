-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 21, 2019 at 05:24 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hs_school`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `title`, `photo`, `description`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Welcome to our school1', '2019/03/about-1-5c7aadf2c10606.8927432312190909_990594194326326_2226378074780795431_n.jpg', '<p>Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of \"de Finibus Bonorum et Malorum\" (The Extremes of Good and Evil) by Cicero, written in 45 BC. This book is a treatise on the theory of ethics, very popular during the Renaissance. The first line of Lorem Ipsum, \"Lorem ipsum dolor sit amet..\", comes from a line in section 1.10.32.</p>\r\n<p>The standard chunk of Lorem Ipsum used since the 1500s is reproduced below for those interested. Sections 1.10.32 and 1.10.33 from \"de Finibus Bonorum et Malorum\" by Cicero are also reproduced in their exact original form, accompanied by English versions from the 1914 translation by H. Rackham.</p>', 1, NULL, '2019-03-06 01:00:48');

-- --------------------------------------------------------

--
-- Table structure for table `committees`
--

CREATE TABLE `committees` (
  `id` int(11) NOT NULL,
  `designation_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(64) NOT NULL,
  `message` text,
  `join_date` datetime DEFAULT '0000-00-00 00:00:00',
  `rejoined_date` datetime DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '1=active, 2=inactive',
  `is_archive` tinyint(4) NOT NULL DEFAULT '1',
  `photo` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `committees`
--

INSERT INTO `committees` (`id`, `designation_id`, `name`, `qualification`, `email`, `phone`, `message`, `join_date`, `rejoined_date`, `status`, `is_archive`, `photo`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(2, 1, 'al amin', 'asdfas', 'admin@gmail.com', '1234567890', 'sdfsd', '2019-01-12 00:00:00', '2019-03-13 00:00:00', 1, 1, '2019/03/about-1234567890-5c893697c7c443.2966842832116496_108490283357849_221695792228859904_n.jpg', '2019-03-12 19:05:56', NULL, '2019-03-17 17:36:52', NULL),
(3, 2, 'al amin1', 'diploma', 'adm1in@gmail.com', '123456789', 'www', '2019-02-14 00:00:00', NULL, 1, 1, '2019/03/about-123456789-5c8935b992b669.2480689232116496_108490283357849_221695792228859904_n.jpg', '2019-03-13 16:54:17', NULL, '2019-03-17 15:38:42', NULL),
(4, 2, 'al amin1', NULL, 'admin@gmail.com', '1234567890', NULL, '2019-01-12 00:00:00', NULL, 1, 1, NULL, '2019-03-13 16:54:36', NULL, '2019-03-13 16:54:36', NULL),
(5, 2, 'daud', NULL, 'daud@gmail.com', '1234567890', NULL, '2019-03-13 00:00:00', NULL, 1, 1, NULL, '2019-03-13 18:59:47', NULL, '2019-03-13 18:59:47', NULL),
(6, 2, 'daud34', NULL, 'daud@gmail.com', '1234567890', NULL, '2019-03-13 00:00:00', NULL, 1, 1, NULL, '2019-03-13 19:08:17', NULL, '2019-03-13 19:08:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE `designations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `is_archive` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` tinyint(4) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `status`, `is_archive`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Chairman', 1, 0, NULL, NULL, '2019-03-18 18:24:06', NULL),
(2, 'Member', 1, 0, '2019-03-09 15:47:14', NULL, '2019-03-12 19:16:29', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `details` text NOT NULL,
  `publish_date` datetime NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_archive` int(11) NOT NULL DEFAULT '0',
  `soft_delete` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `title` varchar(120) NOT NULL,
  `link` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_archive` int(11) NOT NULL DEFAULT '0',
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`id`, `title`, `link`, `status`, `is_archive`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(2, 'NTRC', 'https://app.grammarly.com/ddocs/474767775', 1, 0, NULL, '2019-03-20 17:28:29', NULL, '2019-03-20 17:28:29'),
(4, 'BITM', 'https://www.youtube.com/watch?v=30puTn5AayA', 1, 0, NULL, '2019-03-20 18:29:21', NULL, '2019-03-20 18:29:21');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `message_body` text NOT NULL,
  `message_type` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `is_archive` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `message_body`, `message_type`, `status`, `is_archive`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, '<p><strong>askdjfkasjdfkajs asd</strong></p>', 1, 1, 0, NULL, NULL, NULL, NULL),
(2, '<p>asdfsdfsdfasdfas</p>', 1, 1, 0, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1),
(5, '2019_02_28_150731_create_abouts_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notice_board`
--

CREATE TABLE `notice_board` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `notices` text,
  `pdf` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL,
  `is_archive` tinyint(4) DEFAULT '1',
  `publish_date` datetime DEFAULT '0000-00-00 00:00:00',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `notice_board`
--

INSERT INTO `notice_board` (`id`, `title`, `notices`, `pdf`, `status`, `is_archive`, `publish_date`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'test', '', '', 1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, '2019-03-09 12:00:14', 0),
(2, 'Welcome to our school', '', NULL, 1, 0, '0000-00-00 00:00:00', '2019-03-04 18:48:12', NULL, '2019-03-09 12:02:37', NULL),
(3, 'Welcome to our school', '', NULL, 1, 0, '0000-00-00 00:00:00', '2019-03-04 18:48:33', NULL, '2019-03-13 20:01:12', NULL),
(4, 'Welcome to our school', '', NULL, 1, 1, '0000-00-00 00:00:00', '2019-03-04 18:49:01', NULL, '2019-03-04 18:49:01', NULL),
(5, 'Welcome to our school', NULL, NULL, 1, 1, '2019-03-09 00:00:00', '2019-03-08 06:36:52', NULL, '2019-03-09 10:44:33', NULL),
(6, 'Welcome to our school111111', '<p>asdfasdfasqqqqqqq</p>', '2019/03/about-6-5c83976a6131a2.38146642final-e-commercedocumentation-170202135420.pdf', 1, 1, '2019-03-08 00:00:00', '2019-03-08 07:03:45', NULL, '2019-03-09 10:37:30', NULL),
(7, 'sdfsadfsf', NULL, '2019/03/about--5c86b578254002.58655004Receipt_2019114796.pdf', 2, 1, '2019-03-11 00:00:00', '2019-03-11 19:22:32', NULL, '2019-03-11 19:22:32', NULL),
(8, 'sdfsd', NULL, NULL, 2, 0, '2019-03-11 00:00:00', '2019-03-11 19:28:50', NULL, '2019-03-13 20:00:04', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(11) NOT NULL,
  `designation_id` tinyint(4) NOT NULL,
  `name` varchar(255) NOT NULL,
  `teacher_id` varchar(120) DEFAULT NULL,
  `qualification` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text,
  `join_date` datetime DEFAULT '0000-00-00 00:00:00',
  `rejoined_date` datetime DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `soft_delete` tinyint(4) NOT NULL DEFAULT '1',
  `status` tinyint(4) DEFAULT '1',
  `is_archive` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `designation_id`, `name`, `teacher_id`, `qualification`, `email`, `phone`, `message`, `join_date`, `rejoined_date`, `photo`, `soft_delete`, `status`, `is_archive`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 2, 'daudul', 'asdf33', 'asdf', 'daud@gmail.com', '1234567890', 'asdf', '2019-03-18 00:00:00', '2019-12-31 00:00:00', '2019/03/teacher-1234567890-5c8ff39560ec13.6080841332116496_108490283357849_221695792228859904_n.jpg', 0, 2, 0, '2019-03-18 19:19:17', NULL, '2019-03-18 19:49:13', NULL),
(2, 1, 'samad', '22222', 'diploma', 'daud@gmail.com', '1234567890', NULL, '2019-03-18 00:00:00', NULL, '2019/03/teacher-1234567890-5c913b26224181.9999244132116496_108490283357849_221695792228859904_n.jpg', 1, 1, 0, '2019-03-18 19:39:52', NULL, '2019-03-19 18:55:34', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `te_designations`
--

CREATE TABLE `te_designations` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) DEFAULT '1',
  `is_archive` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `te_designations`
--

INSERT INTO `te_designations` (`id`, `name`, `status`, `is_archive`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Principle', 1, 0, NULL, NULL, '2019-03-18 18:24:41', NULL),
(2, 'Teacher', 1, 0, '2019-03-18 18:13:54', NULL, '2019-03-18 18:13:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'daud', 'daud@gmail.com', '$2y$10$Nv2D4wyi3A8UJU/lMw27wOJ8C/.rF5bWfjki.LZiUTlhdumRGKlAm', 'u332hpXYQI2ouU2ynw1giTkpFJEi0KFau2P1rg45KveAs29Dtrueb9A29VzR', '2019-02-28 23:25:51', '2019-02-28 23:25:51');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `committees`
--
ALTER TABLE `committees`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notice_board`
--
ALTER TABLE `notice_board`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `te_designations`
--
ALTER TABLE `te_designations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `committees`
--
ALTER TABLE `committees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `notice_board`
--
ALTER TABLE `notice_board`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `te_designations`
--
ALTER TABLE `te_designations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
