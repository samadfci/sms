<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/login', 'Auth\AuthController@loginPage')->name('login');
Route::post('/login', 'Auth\AuthController@goLogin')->name('login');

Route::group(['middleware' => 'auth'],function(){
	Route::get('/dashboard', 'DashboardController@dashboardPage')->name('dashboard');
	Route::get('/logout', 'Auth\AuthController@logout')->name('logout');
    
    //about us 
	Route::get('about/about-us','Admin\AboutController@applicationForm')->name('abouts-us');
	Route::post('/update-info','Admin\AboutController@applicationStore')->name('update-info');

	//Notice Board
	Route::get('/notices-list','Admin\NoticeController@List')->name('/notices-list');
	Route::post('/notices/get-notices-list','Admin\NoticeController@getList')->name('/notices-list');
	Route::get('/notices/notices-form','Admin\NoticeController@appForm')->name('notices-form');
	Route::post('/notices/notices-store','Admin\NoticeController@appStore')->name('notices-store');
	Route::post('/notices/upload-document','Admin\NoticeController@uploadDocument');
	Route::get('/notice/edit/{id}','Admin\NoticeController@editForm');
	Route::post('/notices/notices-update','Admin\NoticeController@appStore')->name('notices-update');
	Route::get('notice/notice-open/{id}','Admin\NoticeController@noticeView')->name('notices-open');
	Route::get('notice/delete/{id}','Admin\NoticeController@Delete');

	//governing board
	Route::get('designation-list','Admin\GoverningBoardController@designationList');
	Route::post('get-designation-list','Admin\GoverningBoardController@getDesignationList');
	Route::get('designation-form','Admin\GoverningBoardController@designationForm');
	Route::post('designation-store','Admin\GoverningBoardController@designationStore');
	Route::get('designation/edit/{id}','Admin\GoverningBoardController@editForm');
    //committee 
    Route::get('committee-list','Admin\GoverningBoardController@committeeList');
    Route::get('Committee-form','Admin\GoverningBoardController@committeeForm');
    Route::post('committee-store','Admin\GoverningBoardController@committeeStore');
    Route::post('get-committee-list','Admin\GoverningBoardController@getCommitteeList');
    Route::get('committee/edit/{id}','Admin\GoverningBoardController@committeeEditForm');
    Route::get('committee/committee-open/{id}','Admin\GoverningBoardController@committeeView');
    Route::get('committee/delete/{id}','Admin\GoverningBoardController@committeeDelete');
    Route::get('ex-committee-list','Admin\GoverningBoardController@exCommitteelist');
    Route::post('get-ex-committee-list','Admin\GoverningBoardController@getExCommitteeList');

    //chairman message 
    Route::get('chairman-message','Admin\GoverningBoardController@chairmanMessage');
    Route::post('chairman-message-update','Admin\GoverningBoardController@updateMessage');
    
    //teacher designation 
    Route::get('teacher-designation-list','Admin\TeacherController@designationList');
    Route::post('get-teacher-designation-list','Admin\TeacherController@getDesignationList');
    Route::get('teacher-designation-form','Admin\TeacherController@designationForm');
    Route::post('teacher-designation-store','Admin\TeacherController@designationStore');
    Route::post('teacher-designation-update','Admin\TeacherController@designationStore');
    Route::get('teacher/designation/edit/{id}','Admin\TeacherController@designationFormEdit');

    //teacher section
    Route::get('teacher-list','Admin\TeacherController@teacherList');
    Route::post('get-teacher-list','Admin\TeacherController@getTeacherList');
    Route::get('teacher-form','Admin\TeacherController@teacherForm');
    Route::post('teacher-store','Admin\TeacherController@teacherStore');
    Route::post('teacher-update','Admin\TeacherController@teacherStore');
    Route::get('teacher/edit/{id}','Admin\TeacherController@teacherEditForm');
    Route::get('teacher/teacher-open/{id}','Admin\TeacherController@teacherOpen');
    Route::get('teacher/delete/{id}','Admin\TeacherController@teacherDelete');
    Route::get('ex-teacher-list','Admin\TeacherController@exTeacherList');
    Route::get('ex-teacher-list','Admin\TeacherController@exTeacherList');
    Route::post('get-ex-teacher-list','Admin\TeacherController@getExTeacherList');

    //principal message 
    Route::get('principal-message','Admin\TeacherController@principalMessage');
    Route::post('principal-message-update','Admin\TeacherController@updateMessage');

    //important link
    Route::get('link/link-list','Admin\LinkController@linkList');
    Route::post('link/get-link-list','Admin\LinkController@getLinkList');
    Route::get('/link/link-form','Admin\LinkController@linkForm');
    Route::post('link/link-store','Admin\LinkController@linkStore');
    Route::post('link/link-update','Admin\LinkController@linkStore');
    Route::get('link/edit/{id}','Admin\LinkController@editForm');
    Route::get('link/delete/{id}','Admin\LinkController@delete');

    //event section
    Route::get('event/event-list','Admin\EventController@eventList');
    Route::post('event/get-event-list','Admin\EventController@getEventList');
    Route::get('/event/event-form','Admin\EventController@eventForm');
    Route::post('event/event-store','Admin\EventController@eventStore');
    Route::post('event/event-update','Admin\EventController@eventUpdate');
    Route::get('event/event-edit/{id}','Admin\EventController@editForm');
    Route::get('event/event-open/{id}','Admin\EventController@eventView');
    Route::get('event/delete/{id}','Admin\EventController@Delete');

    //gallery...
    Route::get('gallery/gallery-list','Admin\GalleryController@galleryList');
    Route::get('gallery/gallery-form','Admin\GalleryController@galleryForm');
    Route::post('gallery/gallery-store','Admin\GalleryController@galleryStore');
    Route::post('gallery/get-gallery-list','Admin\GalleryController@getGalleryList');
    Route::get('gallery/gallery/edit/{id}','Admin\GalleryController@galleryEditView');
    Route::post('gallery/gallery-update','Admin\GalleryController@galleryUpdate');
    Route::get('setting/gallery/delete/{id}','Admin\GalleryController@Delete');

    //setting slider image
    Route::get('setting/slider/slider-list', 'Admin\SettingController@sliderList');
    Route::post('setting/slider/get-slider-list', 'Admin\SettingController@getSliderList');
    Route::get('setting/slider/slider-form', 'Admin\SettingController@sliderForm');
    Route::post('slider/slider-store', 'Admin\SettingController@sliderStore');
    Route::get('setting/slider/edit/{id}', 'Admin\SettingController@sliderEditForm');
    Route::post('slider/slider-update', 'Admin\SettingController@sliderUpdate');
    Route::get('setting/slider/delete/{id}', 'Admin\SettingController@sliderDelete');

    //setting
    Route::get('setting/setting-view','Admin\SettingController@settingView');
    Route::post('setting/setting-update','Admin\SettingController@settingUpdate');

    //setting - class
    Route::get('setting/class','Admin\SettingController@classList');
    Route::post('get-class-list','Admin\SettingController@getClassList');
    Route::get('setting/class-form','Admin\SettingController@classForm');
    Route::post('class-store','Admin\SettingController@classStore');
    Route::get('setting/class/edit/{id}','Admin\SettingController@classEditView');
    Route::post('class-update','Admin\SettingController@classStore');
    Route::get('setting/class/delete/{id}','Admin\SettingController@classDelete');

    //class routine
    Route::get('routine/routine-list', 'Admin\RoutineController@routineList');
    Route::get('/routine/routine-form', 'Admin\RoutineController@routineForm');
    Route::post('/routine/routine-store', 'Admin\RoutineController@routineStore');
    Route::post('routine/get-routine-list', 'Admin\RoutineController@getRoutineList');
    Route::get('routine/edit/{id}', 'Admin\RoutineController@routineEditView');
    Route::post('/routine/routine-update', 'Admin\RoutineController@routineEdit');
    Route::get('routine/delete/{id}', 'Admin\RoutineController@Delete');

    //Result type ...
    Route::get('setting/result-type', 'Admin\SettingController@resultTypeList');
    Route::get('setting/result-type-form', 'Admin\SettingController@resultTypeForm');
    Route::post('setting/result-type/store', 'Admin\SettingController@resultTypeStore');
    Route::post('setting/result-type/update', 'Admin\SettingController@resultTypeStore');
    Route::post('get-result-type-list', 'Admin\SettingController@getResultTypeList');
    Route::get('setting/result-type/edit/{id}', 'Admin\SettingController@getResultEditView');

    //result 
    Route::get('result/result-list', 'Admin\ResultController@resultList');
    Route::get('/result/result-form', 'Admin\ResultController@resultForm');
    Route::post('/result/result-store', 'Admin\ResultController@resultStore');
    Route::post('result/get-result-list', 'Admin\ResultController@getResultList');
    Route::get('result/edit/{id}', 'Admin\ResultController@resultEditView');
    Route::post('/result/result-update', 'Admin\ResultController@resultUpdate');
    Route::get('result/delete/{id}', 'Admin\ResultController@Delete');

    //exam routine
    Route::get('exam/exam-routine-list', 'Admin\RoutineController@examRoutineList');
    Route::get('/exam/exam-routine-form', 'Admin\RoutineController@examRoutineForm');
    Route::post('/exam/exam-routine-store', 'Admin\RoutineController@examRoutineStore');
    Route::post('exam/get-exam-routine-list', 'Admin\RoutineController@getExamRoutineList');
    Route::get('exam/exam-routine-edit/{id}', 'Admin\RoutineController@examRoutineEditView');
    Route::post('/exam/exam-routine-update', 'Admin\RoutineController@examRoutineUpdate');
    Route::get('exam/exam-routine-delete/{id}', 'Admin\RoutineController@examRoutineDelete');

    //adcademin calendar
    Route::get('calendar/calendar-list', 'Admin\CalendarController@calendarList');
    Route::post('calendar/get-calendar-list', 'Admin\CalendarController@getCalendarList');
    Route::get('calendar/calendar-form', 'Admin\CalendarController@calendarForm');
    Route::post('calendar/calendar-store', 'Admin\CalendarController@calendarStore');
    Route::get('calender/calender-edit/{id}', 'Admin\CalendarController@calendarEditView');
    Route::post('calendar/calendar-update', 'Admin\CalendarController@calendarUpdate');
    Route::get('calender/calender-delete/{id}', 'Admin\CalendarController@calendarDelete');

    //contact ...
    Route::get('setting/Contact-view','Admin\SettingController@contactView');
    Route::post('/setting/contact-update','Admin\SettingController@contactUpdate');

});

Route::get('/','Frontend\FrontendController@index');
Route::get('about-us','Frontend\PageController@aboutUs');
//class routine
Route::get('class-routine','Frontend\PageController@classRoutine');
Route::post('class-routine-list','Frontend\PageController@getClassRoutine');

//exam routine
Route::get('exam-routine','Frontend\PageController@examRoutine');
Route::post('exam-routine','Frontend\PageController@getExamRoutine');
//exam result
Route::get('exam-result','Frontend\PageController@examResult');
Route::post('exam-result','Frontend\PageController@getExamResult');

//committee section
Route::get('governing-body','Frontend\PageController@governingBody');
Route::post('get-governing-body','Frontend\PageController@getGoverningBody');
Route::post('profile/{id}','Frontend\PageController@Profile');

//teachers and staff section
Route::get('teachers-staff','Frontend\PageController@teachersStaff');
Route::post('get-teachers-staff','Frontend\PageController@getTeachersStaff');
Route::post('teacher-profile/{id}','Frontend\PageController@teacherProfile');
//ex-teachers and staff section
Route::get('ex-teachers-staff','Frontend\PageController@ExTeachersStaff');
Route::post('get-ex-teachers-staff','Frontend\PageController@getExTeachersStaff');
Route::post('ex-teacher-profile/{id}','Frontend\PageController@ExTeacherProfile');

//ex-committee section
Route::get('ex-governing-body','Frontend\PageController@exGoverningBody');
Route::post('get-ex-governing-body','Frontend\PageController@getExGoverningBody');
Route::post('ex-profile/{id}','Frontend\PageController@exProfile');

//chairman message section
Route::get('view-chairman-message','Frontend\PageController@getChairmanMessage');
Route::get('view-principal-message','Frontend\PageController@getPrincipalMessage');

//notice ..
Route::get('view-notice','Frontend\PageController@allNotice');
Route::post('all-notice-list','Frontend\PageController@getAllNotice');
Route::get('single-notice-open/{id}','Frontend\PageController@singleView');

//Event ..
Route::get('view-event','Frontend\PageController@allEvent');
Route::post('all-event-list','Frontend\PageController@getAllEvent');
Route::get('single-event-open/{id}','Frontend\PageController@singleEvent');

//academin calendar ...
Route::get('academic-calendar', 'Frontend\PageController@Calendar');
Route::post('show-calendar-list', 'Frontend\PageController@academicCalendarList');

Route::get('contact','Frontend\PageController@Contact');
Route::get('view-gallary','Frontend\PageController@allGallary');
Route::post('show-img/{id}','Frontend\PageController@showImg');



