<?php 

use Illuminate\Support\Facades\Auth;

Class CommonFunction 

{
  public static function getUserId() {

        if (Auth::user()) {
            return Auth::user()->id;
        } else {
            return 'Invalid Login Id';
        }
    }
}