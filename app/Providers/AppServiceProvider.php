<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Link;
use App\Models\Setting;
use App\Models\Committee;
use App\Models\Teacher;
use View;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
          View::composer('frontEnd.includes.footer',function($view){
           $contact = DB::table('contact')->where('id', 1)->first();
           $view->with('contact',$contact);
        });

        View::composer('frontEnd.layouts',function($view){
           $importantLink = Link::Where('status',1)->orderBy('id', 'DESC')->limit(15)->get();
           $view->with('importantLink',$importantLink);
        });
        
        View::composer('frontEnd.includes.footer',function($view){
           $setting = Setting::find('1');
           $view->with('setting',$setting);
        });

        View::composer('frontEnd.home',function($view){
           $setting = Setting::find('1');
           $view->with('setting',$setting);
        });

        View::composer('frontEnd.layouts',function($view){
           //chairman info
            $chairman = Committee::join('designations', 'designations.id', '=', 'committees.designation_id')
                   ->join('messages', 'messages.message_type', '=', 'committees.designation_id')
                   ->where('committees.status', 1)
                   ->where('designations.status', 1)
                   ->where('messages.status', 1)
                   ->first(['committees.name as chairman_name','committees.photo','designations.name as designation']);
           $view->with('chairman',$chairman);
        });

        View::composer('frontEnd.layouts',function($view){
           //principal 
        $principal = Teacher::join('te_designations', 'te_designations.id', '=', 'teachers.designation_id')
                   ->join('messages', 'messages.message_type', '=', 'teachers.designation_id')
                   ->where('teachers.status', 1)
                   ->where('te_designations.status', 1)
                   ->where('messages.status', 1)
                   ->first(['teachers.name as chairman_name','teachers.photo','te_designations.name as designation']);
         $view->with('principal',$principal);
        });   
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
