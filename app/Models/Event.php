<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
	protected $table = 'events';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id',
        'title',
        'details',
        'publish_date',
        'photo',
        'status',
        'is_archive'
    ];

    public static function List()
    {
        return Event::where(array('soft_delete' => '0', ))->orderBy('id', 'desc')->get();
    }

}
