<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Routine extends Model
{
	protected $table = 'class_routine';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id',
         'class_id',
         'title',
         'pdf_file',
         'year',
         'publish_date',
         'status',
         'is_archive'

    ];

}
