<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
	protected $table = 'academic_calendars';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id',
         'title',
         'pdf_file',
         'year',
         'publish_date',
         'status',
         'is_archive'

    ];

}
