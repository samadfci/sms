<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Notice extends Model
{

    protected $table = 'notice_board';

    protected $fillable = array(
        'id',
        'title',
        'pdf',
        'status',
        'notices',
        'publish_date',
        'is_archive',
        'created_by',
        'updated_by',
        'created_at',
        'updated_at',
    );

    public static function List()
    {
        return Notice::where(array('is_archive' => '1'))->orderBy('id', 'desc')
            ->get(['id','title','pdf','publish_date','status']);
    }

}
