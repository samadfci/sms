<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ResultType extends Model
{
	protected $table = 'result_type';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id',
        'name'
    ];

}
