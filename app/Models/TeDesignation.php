<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeDesignation extends Model
{
	protected $table = 'te_designations';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    	'id',
        'name'
    ];

}
