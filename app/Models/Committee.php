<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Committee extends Model
{
	protected $table = 'committees';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id',
         'designation_id',
         'name',
         'qualification',
         'email',
         'phone',
         'message',
         'join_date',
         'rejoined_date',
         'status',
         'is_archive'

    ];

}
