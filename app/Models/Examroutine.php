<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Examroutine extends Model
{
	protected $table = 'exam_routine';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id',
         'class_id',
         'pdf_file',
         'year',
         'publish_date',
         'status',
         'is_archive'

    ];

}
