<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
	protected $table = 'teachers';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id',
         'designation_id',
         'name',
         'teacher_id',
         'qualification',
         'email',
         'phone',
         'message',
         'join_date',
         'rejoined_date',
         'status',
         'is_archive'
    ];

}
