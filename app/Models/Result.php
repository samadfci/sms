<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Result extends Model
{
	protected $table = 'results';
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'id',
         'class_id',
         'result_type_id',
         'title',
         'result_pdf',
         'year',
         'publish_date',
         'status',
         'is_archive'

    ];

}
