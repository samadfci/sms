<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Auth;

class AuthController extends Controller
{

	public function loginPage(){
		return view('admin.login');
	}

	public function goLogin(Request $request){
          
          $inputs = $request->except('_token');
          $validator = Validator::make($inputs,[
            'email' => 'required|email',
            'password' => 'required|min:6'
           
          ]);  

          if ($validator->fails()) {
          	return redirect()->back()->withErrors($validator)->withInput();
          }

          if (Auth::attempt($inputs)) {
          	  session()->flash('successMsg','You has been logged in');
          	  return redirect()->route('dashboard');
          }

          session()->flash('errorMsg','Wrong Credential !');

          return redirect()->back();
	}

	public function logout(){
		Auth::logout();
        session()->flash('successMsg','You have been logout.');
        return redirect()->route('login');

	}
    
}
