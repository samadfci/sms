<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\Notice;
use App\Models\Event;
use DB;

class DashboardController extends Controller
{
    public function dashboardPage(){

    	$total_teacher = $data = DB::table('teachers')
            ->join('te_designations', 'teachers.designation_id', '=', 'te_designations.id')
            ->where('teachers.soft_delete', 1)
            ->where('teachers.status', 1)
            ->select('teachers.*', 'te_designations.name as designation')
            ->count();

        $committee = DB::table('committees')
            ->join('designations', 'committees.designation_id', '=', 'designations.id')
            ->where('committees.is_archive', 1)
            ->where('committees.status', 1)
            ->select('committees.*', 'designations.name as designation')
            ->count();

        $notice = Notice::where(array('is_archive' => '1', 'status' => '1'))->orderBy('id', 'desc')
            ->count();

        $event = Event::where(array('soft_delete' => '0', 'status' => '1'))->orderBy('id', 'desc')->count();            

    	return view('admin.dashboard', compact('total_teacher', 'committee', 'notice', 'event'));
    }
}
