<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use App\Models\Link;
use Crypt;
use DB;
use Session;

class LinkController extends Controller
{
    public function linkList()
    {
    	return view('admin.link.link-list');
    }

    public function getLinkList()
    {
    	$link = Link::where(array('status' => '1'))->orderBy('id', 'desc')
            ->get(['id','title','link','status']);

        return Datatables::of($link)
            ->editColumn('sl_no', function ($link) {
                return '';
            }) 
            ->editColumn('action', function ($link) {
                $html = '';
                    $html = ' <a href="' . url('link/edit/'. Crypt::encrypt($link->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('link/delete/' . Crypt::encrypt($link->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
         
            })
            ->addColumn('status', function ($link) {
                if($link->status == 1){
                   return "<label class='btn btn-success btn-sm'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Inactive</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function linkForm()
    {
    	return view('admin.link.link-form');
    }

    public function linkStore(Request $request)
    {
    	$this->validate($request,[
           'title' => 'required',
           'link' => 'required'
    	]);
        
        try{
        DB::beginTransaction();
    	if (!empty($request->id)) {
    		$link = Link::find($request->id);
    	}else{
    		$link = new Link();
    	}

    	$link->title = $request->title;
    	$link->link = $request->link;
    	if (!is_null($request->status)){
    		$link->status = $request->status;
    	}
    	$link->save();

    	DB::commit();
    	Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
    }catch(Exception $e){
    	DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
        //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();
    }

    }

    public function editForm($id)
    {
    	$editLink = Link::find(Crypt::decrypt($id));
        return view('admin.link.link-form-edit',compact('editLink'));
    }

    public function delete($id)
    {
        $link = Link::find(Crypt::decrypt($id));
        $link->delete();

        Session::flash('success', "Data Has Been Deleted Successfully");
        return redirect()->back(); 
    }


}
