<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Gallery;
use yajra\Datatables\Datatables;
use Crypt;
use DB;
use Session;

class GalleryController extends Controller
{
    public function galleryList()
    {
    	return view('admin.gallery.gallery-list');
    }

    public function galleryForm()
    {
    	return view('admin.gallery.gallery-form');
    }

    public function galleryStore(Request $request)
    {
		$this->validate($request,[
       'photo' => 'required|mimes:jpeg,jpg,png,gif|max:30000',
	   ]);
        try{
          $gallery = new Gallery();
    	    $gallery->title = $request->title;
    	if ($request->hasFile('photo')) {
           $yearMonth = date("Y") . "/" . date("m") . "/";
           $path = 'uploads/' . $yearMonth;
           if (!file_exists($path)) {
               mkdir($path, 0777, true);
           }
           $_file_path = $request->file('photo');
           $file_path = trim(uniqid('gallery-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
           $_file_path->move($path, $file_path);
           $gallery->photo = $yearMonth . $file_path;
        }

        $gallery->save();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
        }catch(\Exception $e){
            Session::flash('error', 'Some thing went wrong (ErrorCode : gallery101)');
        dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();
        }
    }

    public function getGalleryList()
    {
      $gallery = Gallery::get();
      return Datatables::of($gallery)
            ->editColumn('sl_no', function ($gallery) {
                return '';
            }) 
            ->editColumn('action', function ($gallery) {
                $html = '';
                    $html = ' <a href="' . url('gallery/gallery/edit/'. Crypt::encrypt($gallery->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('setting/gallery/delete/' . Crypt::encrypt($gallery->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('photo', function($gallery){
              
              if (!empty($gallery->photo)) {
                  return '<img src="/uploads/'.$gallery->photo.'" height="180px" width="220px">';
               }else{
                 return '';
               }
            })
            ->addColumn('status', function ($gallery) {
                if($gallery->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Un Publish</label>";
               }
            })
            ->rawColumns(['status', 'action','photo'])
            ->make(true);
    }

    public function galleryEditView($id)
    {
        $gallery = Gallery::find(Crypt::decrypt($id));
        return view('admin.gallery.gallery-form-edit',compact('gallery'));
    }

    public function galleryUpdate(Request $request)
    {
       if (!is_null($request->photo)) {
       $this->validate($request,[
         'photo' => 'required|mimes:jpeg,jpg,png,gif|max:30000',
       ]);
       }
        try{
          $gallery = Gallery::find($request->id);
          $gallery->title = $request->title;
      if ($request->hasFile('photo')) {
           $yearMonth = date("Y") . "/" . date("m") . "/";
           $path = 'uploads/' . $yearMonth;
           if (!file_exists($path)) {
               mkdir($path, 0777, true);
           }
           $_file_path = $request->file('photo');
           $file_path = trim(uniqid('gallery-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
           $_file_path->move($path, $file_path);
           $gallery->photo = $yearMonth . $file_path;
        }
        $gallery->status = $request->status;
        $gallery->save();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
        }catch(\Exception $e){
            Session::flash('error', 'Some thing went wrong (ErrorCode : gallery101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();
        }
    }

    public function Delete($id)
    {
      Gallery::where('id', Crypt::decrypt($id))->delete();
      Session::flash('success', "Data Has Been Deleted Successfully");
      return redirect()->back();
    }
}
