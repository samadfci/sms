<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use App\Models\Setting;
use App\Models\Sclass;
use App\Models\ResultType;
use yajra\Datatables\Datatables;
use Crypt;
use DB;
use Session;

class SettingController extends Controller
{
    public function sliderList()
    {
    	return view('admin.setting.slider.slider-list');
    }

    public function sliderForm()
    {
    	return view('admin.setting.slider.slider-form');
    }

    public function sliderStore(Request $request)
    {
    	
    		$this->validate($request,[
           'photo' => 'required|mimes:jpeg,jpg,png,gif|max:30000',
    	   ]);
    	
    	if (!is_null($request->id)) {
    		$slider = Slider::find($request->id);
    	}else{
    		$slider = new Slider();
    	}
        try{
    	$slider->title = $request->title;
    	if ($request->hasFile('photo')) {
           $yearMonth = date("Y") . "/" . date("m") . "/";
           $path = 'uploads/' . $yearMonth;
           if (!file_exists($path)) {
               mkdir($path, 0777, true);
           }
           $_file_path = $request->file('photo');
           $file_path = trim(uniqid('slider-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
           $_file_path->move($path, $file_path);
           $slider->photo = $yearMonth . $file_path;
        }
        if (!is_null($request->status)) {
        	$slider->status = $request->status;
        }
        $slider->save();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
        }catch(\Exception $e){
            Session::flash('error', 'Some thing went wrong (ErrorCode : slider101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();
        }
    }

    public function sliderUpdate(Request $request)
    {
       if (!is_null($request->photo)) {
        $this->validate($request,[
           'photo' => 'required|mimes:jpeg,jpg,png,gif|max:30000',
         ]);
       }
        
      
      if (!is_null($request->id)) {
        $slider = Slider::find($request->id);
      }else{
        $slider = new Slider();
      }
        try{
      $slider->title = $request->title;
      if ($request->hasFile('photo')) {
           $yearMonth = date("Y") . "/" . date("m") . "/";
           $path = 'uploads/' . $yearMonth;
           if (!file_exists($path)) {
               mkdir($path, 0777, true);
           }
           $_file_path = $request->file('photo');
           $file_path = trim(uniqid('slider-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
           $_file_path->move($path, $file_path);
           $slider->photo = $yearMonth . $file_path;
        }
        if (!is_null($request->status)) {
          $slider->status = $request->status;
        }
        $slider->save();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
        }catch(\Exception $e){
            Session::flash('error', 'Some thing went wrong (ErrorCode : slider101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();
        }
    }

    public function getSliderList()
    {
    	$slider = Slider::get();
    	return Datatables::of($slider)
            ->editColumn('sl_no', function ($slider) {
                return '';
            }) 
            ->editColumn('action', function ($slider) {
                $html = '';
                    $html = ' <a href="' . url('setting/slider/edit/'. Crypt::encrypt($slider->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('setting/slider/delete/' . Crypt::encrypt($slider->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('photo', function($slider){
              
              if (!empty($slider->photo)) {
                  return '<img src="/uploads/'.$slider->photo.'" height="180px" width="220px">';
               }else{
                 return '';
               }
            })
            ->addColumn('status', function ($slider) {
                if($slider->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Un Publish</label>";
               }
            })
            ->rawColumns(['status', 'action','photo'])
            ->make(true);
    }

    public function sliderEditForm($id)
    {   
    	$slider = Slider::find(Crypt::decrypt($id));
    	return view('admin.setting.slider.slider-form-edit',compact('slider'));
    }

    public function sliderDelete($id)
    {
       Slider::where('id', Crypt::decrypt($id))->delete();
      Session::flash('success', "Data Has Been Deleted Successfully");
      return redirect()->back();
    }

    public function settingView()
    {
      $setting = DB::table('settings')->where('id', 1)->first();
      return view('admin.setting.setting-form',compact('setting'));
    }

    public function settingUpdate(Request $request)
    {
        
            $this->validate($request,[
                'photo' => 'mimes:jpeg,jpg,png,gif|max:30000' 
            ]);

            try{
            DB::beginTransaction();
            $setting = Setting::find(1);

            if ($request->hasFile('photo')) {
               $yearMonth = date("Y") . "/" . date("m") . "/";
               $path = 'uploads/' . $yearMonth;
               if (!file_exists($path)) {
                   mkdir($path, 0777, true);
               }
               $_file_path = $request->file('photo');
               $file_path = trim(uniqid('logo-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
               $_file_path->move($path, $file_path);
               $setting->logo = $yearMonth . $file_path;
            }
            $setting->total_teacher = $request->total_teacher;
            $setting->total_student = $request->total_student;
            $setting->total_committee = $request->total_committee;
            $setting->facebook = $request->facebook;
            $setting->youtube = $request->youtube;
            $setting->google = $request->google;
            $setting->copy_right = $request->copy_right;
            $setting->save();

            DB::commit();
            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();

           }catch (\Exception $e) {
                DB::rollback();
                Session::flash('error', 'Some thing went wrong (ErrorCode : S-102)');
                //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
                return redirect()->back();
            }
          }

          public function classList()
          {
            return view('admin.setting.class.class-list');
          }

          public function classForm()
          {
            return view('admin.setting.class.class-form');
          }

          public function classStore(Request $request)
          {
              $this->validate($request, [
                 'name' => 'required'
              ]);
              
              $check = Sclass::where('name', $request->name)->where('status', 1)->first();
               
              if ($request->id) {
                $class = Sclass::find($request->id);
                if ($check) {
                 Session::flash('error', "This name already exists, please type another name.");
                 $class->status = $request->status;
                 $class->save();
                 return redirect()->back();
               }
              }else{
               if ($check) {
                 Session::flash('error', "This name already exists, please type another name.");
                 return redirect()->back();
               }
                $class = new Sclass();
              }

              $class->name = $request->name;
              if (!is_null($request->status)) {
                $class->status = $request->status; 
              }
              $class->save();

              Session::flash('success', "Data Has Been Stored Successfully");
              return redirect()->back(); 
          }

          public function getClassList()
          {
            $class = Sclass::get();
            return Datatables::of($class)
            ->editColumn('sl_no', function ($class) {
                return '';
            }) 
            ->editColumn('action', function ($class) {
                $html = '';
                    $html = ' <a href="' . url('setting/class/edit/'. Crypt::encrypt($class->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('setting/class/delete/' . Crypt::encrypt($class->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })
            ->addColumn('status', function ($class) {
                if($class->status == 1){
                   return "<label class='btn btn-success btn-sm'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Inactive</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
          }

          public function classEditView($id)
          {
          
           $class = Sclass::find(Crypt::decrypt($id));
           return view('admin.setting.class.class-form-edit', compact('class'));
          }

          public function classDelete($id)
          {
            Sclass::where('id', Crypt::decrypt($id))->delete();
            Session::flash('success', "Data Has Been Deleted Successfully");
            return redirect()->back(); 
          }

          public function resultTypeList()
          {
            return view('admin.setting.result-type.result-type-list');
          }

          public function resultTypeForm()
          {
            return view('admin.setting.result-type.result-type-form');
          }

          public function resultTypeStore(Request $request)
          {
              $this->validate($request, [
                 'name' => 'required'
              ]);
              $check = ResultType::where('name', $request->name)->where('status', 1)->first();
              if ($request->id) {
                $result_type = ResultType::find($request->id);
                if ($check) {
                 Session::flash('error', "This name already exists, please type another name.");
                 $result_type->status = $request->status;
                 $result_type->save();
                 return redirect()->back();
               }
              }else{
               if ($check) {
                 Session::flash('error', "This name already exists, please type another name.");
                 return redirect()->back();
               }
                $result_type = new ResultType();
              }

              $result_type->name = $request->name;
              if (!is_null($request->status)) {
                $result_type->status = $request->status; 
              }
              $result_type->save();

              Session::flash('success', "Data Has Been Stored Successfully");
              return redirect()->back();   
          }

          public function getResultTypeList()
          {
            $result_type = ResultType::get();
            return Datatables::of($result_type)
            ->editColumn('sl_no', function ($result_type) {
                return '';
            }) 
            ->editColumn('action', function ($result_type) {
                $html = '';
                    $html = ' <a href="' . url('setting/result-type/edit/'. Crypt::encrypt($result_type->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('setting/result-type/delete/' . Crypt::encrypt($result_type->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })
            ->addColumn('status', function ($result_type) {
                if($result_type->status == 1){
                   return "<label class='btn btn-success btn-sm'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Inactive</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
          }

          public function getResultEditView($id)
          {
            
            $result_type = ResultType::find(Crypt::decrypt($id));
            return view('admin.setting.result-type.result-type-form-edit',compact('result_type'));
          }

          public function contactView()
          {
            $contact = DB::table('contact')->where('id', 1)->first();
            return view('admin.setting.contact.contact', compact('contact'));
          }

          public function contactUpdate(Request $request)
          {
            $contact = DB::table('contact')->where('id', 1)->update([
              'address' => $request->address,
              'phone' => $request->phone,
              'email' => $request->email,
              'map' => $request->map,
            ]);

            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();
          }
}
