<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use App\Models\TeDesignation;
use App\Models\Teacher;
use Crypt;
use DB;
use Session;
use Carbon\Carbon;

class TeacherController extends Controller
{
	public function designationList()
	{
       return view('admin.teacher.designation-list');
	}

	public function getDesignationList()
	{
		$designation = TeDesignation::get();
        return Datatables::of($designation)
            ->editColumn('sl_no', function ($designation) {
                return '';
            }) 
            ->editColumn('action', function ($designation) {
                $html = '';
                    $html = ' <a href="' . url('teacher/designation/edit/'. Crypt::encrypt($designation->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                return $html;
            })
            ->addColumn('status', function ($designation) {
                if($designation->status == 1){
                   return "<label class='btn btn-success btn-sm'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>In Active</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
	}

	public function designationForm()
	{
		return view('admin.teacher.designation-form');
	}

	public function designationStore(Request $request)
	{
		$this->validate($request,[
                'name' => 'required'
            ]);
            
            try{

            DB::beginTransaction();
            if (!empty($request->id)) {
              $designation = TeDesignation::find($request->id);
            }else{
              $designation = TeDesignation::where('name', $request->name)->first();
              if ($designation) {
                Session::flash('error', "This name already exists !");
                return redirect()->back();  
              }else{
                $designation = new TeDesignation();
                
              }
            }

            $designation->name = $request->name;
            $designation->status = $request->status;
            $designation->save();
            DB::commit();
            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();

           }catch (Exception $e) {
                DB::rollback();
                Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
                //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
                return redirect()->back();
            }
	}

	public function designationFormEdit($id)
	{
		$designation = TeDesignation::find(Crypt::decrypt($id));
		return view('admin.teacher.designation-form-edit',compact('designation'));
	}

    public function teacherList()
    {
    	return view('admin.teacher.teacher-list');
    }

    public function getTeacherList()
    {
    	$data = DB::table('teachers')
            ->join('te_designations', 'teachers.designation_id', '=', 'te_designations.id')
            ->where('te_designations.status', 1)
            ->where('teachers.soft_delete', 1)
            ->where('teachers.status', 1)
            ->select('teachers.*', 'te_designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($notice) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a href="' . url('teacher/teacher-open/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                    $html .= ' <a href="' . url('teacher/edit/'. Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('teacher/delete/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->addColumn('status', function ($data) {
                if($data->status == 1){
                   return "<label class='btn btn-success btn-xs'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-xs'>Inactive</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function teacherForm()
    {
    	$designation = TeDesignation::where('status', 1)->get(['name','id']);
    	return view('admin.teacher.teacher-form',compact('designation'));
    }

    public function teacherStore(Request $request)
    {
    	 $this->validate($request,[
                'name' => 'required',
                'designation_id' => 'required|not_in:0',
                'phone' => 'required',
                'join_date' => 'required' 
            ]);

         try{
            DB::beginTransaction();
           if ($request->id) {
               $teacher = Teacher::find($request->id);
           }else{
              $teacher = new Teacher();
           }

           $teacher->designation_id = $request->designation_id;
           $teacher->teacher_id = $request->teacher_id;
           $teacher->name = $request->name;
           $teacher->qualification = $request->qualification;
           $teacher->email = $request->email;
           $teacher->phone = $request->phone;
           $teacher->gender = $request->gender;
           $teacher->address = $request->address;
           $teacher->join_date = date('Y-m-d', strtotime($request->get('join_date')));
           if (!empty($request->rejoined_date)) {
            $teacher->rejoined_date = date('Y-m-d', strtotime($request->get('rejoined_date')));
           }
           if ($request->hasFile('photo')) {
               $yearMonth = date("Y") . "/" . date("m") . "/";
               $path = 'uploads/' . $yearMonth;
               if (!file_exists($path)) {
                   mkdir($path, 0777, true);
               }
               $_file_path = $request->file('photo');
               $file_path = trim(uniqid('teacher-' . $request->phone . '-', true) . $_file_path->getClientOriginalName());
               $_file_path->move($path, $file_path);
               $teacher->photo = $yearMonth . $file_path;
            }
           $teacher->status = $request->status;
           $teacher->message = $request->message;
           $teacher->save();

            DB::commit();
            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();

         }catch (Exception $e){
            DB::rollback();
            Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
            //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
            return redirect()->back();
         }
    }

    public function teacherEditForm($id)
    {
      $designation = TeDesignation::where('status', 1)->get(['name','id']);
      $teacher = Teacher::find(Crypt::decrypt($id));
      $viewMode = 'on';
    	return view('admin.teacher.teacher-form-edit',compact('designation','teacher','viewMode'));
    }

    public function teacherOpen($id)
    {
      $designation = TeDesignation::where('status', 1)->get(['name','id']);
      $teacher = Teacher::find(Crypt::decrypt($id));
      $viewMode = 'off';
      
      return view('admin.teacher.teacher-form-edit',compact('designation','teacher','viewMode'));
    }

    public function teacherDelete($id)
    {
      $teacher = Teacher::find(Crypt::decrypt($id));
      $teacher->soft_delete = 0;
      $teacher->status = 2;
      $teacher->save();

      Session::flash('success', "Data Has Been Delete Successfully");
      return redirect()->back();
    }

    public function principalMessage()
    {
    	$message = Teacher::join('messages', 'messages.message_type', '=', 'teachers.designation_id')
           ->where('teachers.status', 1)
           ->where('messages.status', 1)
           ->where('messages.id', 2)
           ->first(['teachers.designation_id','messages.*']);
           
      return view('admin.teacher.principal-message',compact('message'));
    }

    public function updateMessage(Request $request)
    {
      $this->validate($request,[
                'message_body' => 'required',
                'message_type' => 'required'
            ]);
      $message = DB::table('messages')
                     ->where('id', $request->id)
                     ->where('message_type', $request->message_type)
                     ->update([
                        'message_body' => $request->message_body,
                        'message_type' => $request->message_type,
                        'status'       => $request->status,
                    ]);

      Session::flash('success', "Data Has Been Update Successfully");
      return redirect()->back();               
    }

    public function exTeacherList()
    {
      return view('admin.teacher.ex-teacher-list');
    }

    public function getExTeacherList()
    {
      $data = DB::table('teachers')
            ->join('te_designations', 'teachers.designation_id', '=', 'te_designations.id')
            ->where('teachers.soft_delete', 1)
            ->where('teachers.status', 2)
            ->select('teachers.*', 'te_designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($data) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a href="' . url('teacher/teacher-open/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                    $html .= ' <a href="' . url('teacher/edit/'. Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('teacher/delete/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->addColumn('status', function ($data) {
                if($data->status == 1){
                   return "<label class='btn btn-success btn-xs'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-xs'>Inactive</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }
}
