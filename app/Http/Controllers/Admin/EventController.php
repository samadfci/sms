<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use App\Models\Event;
use Crypt;
use DB;
use Session;

class EventController extends Controller
{
    public function eventList()
    {
    	return view('admin.event.event-list');
    }

    public function getEventList()
    {
    	$event = Event::List();
        return Datatables::of($event)
            ->editColumn('sl_no', function ($event) {
                return '';
            }) 
            ->editColumn('action', function ($event) {
                $html = '';
                    $html = '<a href="' . url('event/event-open/' . Crypt::encrypt($event->id)) . '" class="btn btn-sm btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                    $html .= ' <a href="' . url('event/event-edit/'. Crypt::encrypt($event->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('event/delete/' . Crypt::encrypt($event->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('publish_date', function($event){
              if (!is_null($event->publish_date)) {
                return date('Y-m-d', strtotime($event->publish_date));
              }
              return '';
            })->addColumn('title',function($event){
            	return substr($event->title, 0, 60);
            })
            ->addColumn('status', function ($event) {
                if($event->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Un Publish</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function eventForm()
    {
    	return view('admin.event.event-form');
    }

    public function eventStore(Request $request)
    {
    	$this->validate($request,[
            'title' => 'required',
            'details' => 'required',
            'publish_date' => 'required' 
    	]);

    	try{
        DB::beginTransaction();
		if (!is_null($request->id)) {
		    $event = Event::find($request->id);    	
        }else{
        	$event = new Event();
        }
        $event->title = $request->title; 
        $event->details = $request->details;

        if ($request->hasFile('photo')) {
           $yearMonth = date("Y") . "/" . date("m") . "/";
           $path = 'uploads/' . $yearMonth;
           if (!file_exists($path)) {
               mkdir($path, 0777, true);
           }
           $_file_path = $request->file('photo');
           $file_path = trim(uniqid('about-' . date("i") . '-', true) . $_file_path->getClientOriginalName());
           $_file_path->move($path, $file_path);
           $event->photo = $yearMonth . $file_path;
           }
           
           $event->publish_date = date('Y-m-d', strtotime($request->get('publish_date')));
           if (!is_null($request->status)) {
           $event->status = $request->status;
           }
           $event->save();

           DB::commit();
           Session::flash('success', "Data Has Been Stored Successfully");
           return redirect()->back(); 
    	}catch(Exception $e){
            DB::rollback();
            Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
            //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
            return redirect()->back();
    	}
    }

    public function editForm($id)
    {
    	$event = Event::find(Crypt::decrypt($id));
    	$viewMode = 'on';
    	return view('admin.event.event-form-edit',compact('event','viewMode'));
    }

    public function eventView($id)
    {
        $event = Event::find(Crypt::decrypt($id));
        $viewMode = 'off';
    	return view('admin.event.event-form-edit',compact('event','viewMode'));
    }

    public function Delete($id)
    {
    	$event = Event::find(Crypt::decrypt($id));
    	$event->soft_delete = 1;
      $event->status=0;
    	$event->save();

       Session::flash('success', "Data Has Been Deleted Successfully");
       return redirect()->back(); 

    }

}
