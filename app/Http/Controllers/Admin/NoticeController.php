<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use App\Models\Notice;
use Crypt;
use DB;
use Session;

class NoticeController extends Controller
{
    public function List()
    {
    	return view('admin.notice.noticeList');
    }

    public function getList()
    {
      
    	$notice = Notice::List();
        return Datatables::of($notice)
            ->editColumn('sl_no', function ($notice) {
                return '';
            }) 
            ->editColumn('action', function ($notice) {
                $html = '';
                    $html = '<a href="' . url('notice/notice-open/' . Crypt::encrypt($notice->id)) . '" class="btn btn-sm btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                    $html .= ' <a href="' . url('notice/edit/'. Crypt::encrypt($notice->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('notice/delete/' . Crypt::encrypt($notice->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('pdf', function($notice){
              
              if (!empty($notice->pdf)) {
                  return '<a href="/uploads/'.$notice->pdf.'" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-folder-open-o"></i> Open File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($notice){
              if ($notice->publish_date != '0000-00-00') {
                return date('Y-m-d', strtotime($notice->publish_date));
              }
            })
            ->addColumn('status', function ($notice) {
                if($notice->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Un Publish</label>";
               }
            })
            ->rawColumns(['status', 'action','pdf'])
            ->make(true);
    }

    public function appForm()
    {

        return view('admin.notice.appForm');
    }

    public function appStore(Request $request)
    {
            $this->validate($request,[
                'title' => 'required',
                'publish_date' => 'required' 
            ]);

            try{
            DB::beginTransaction();

            if (!empty($request->id)) {
              $notice = Notice::find($request->id);
            }else{
              $notice = new Notice();
            }
            
            $notice->title = $request->title;
            $notice->notices = $request->notices;

            if ($request->hasFile('pdf')) {
           $yearMonth = date("Y") . "/" . date("m") . "/";
           $path = 'uploads/' . $yearMonth;
           if (!file_exists($path)) {
               mkdir($path, 0777, true);
           }
           $_file_path = $request->file('pdf');
           $file_path = trim(uniqid('about-' . $request->id . '-', true) . $_file_path->getClientOriginalName());
           $_file_path->move($path, $file_path);
           $notice->pdf = $yearMonth . $file_path;
            }
            $notice->publish_date = date('Y-m-d', strtotime($request->get('publish_date')));
            $notice->status = $request->status;
            $notice->save();

            DB::commit();
            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();

           }catch (\Exception $e) {
                DB::rollback();
                Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
                //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
                return redirect()->back();
            }
    }

    public function uploadDocument()
    {
        return view('ajaxUploadFile');
    }

    public function editForm($id)
    {
      $viewMode = 'on';
      $notice = Notice::find(Crypt::decrypt($id));
      return view('admin.notice.appForm-Edit',compact('notice','viewMode'));
    }

    public function noticeView($id)
    {
      $viewMode = 'off';
      $notice = Notice::find(Crypt::decrypt($id));
      return view('admin.notice.appForm-Edit',compact('notice','viewMode')); 
    }

    public function Delete($id)
    {
      $notice = Notice::find(Crypt::decrypt($id));
      $notice->is_archive = 0;
      $notice->status = 0;
      $notice->save();

      Session::flash('success', "Data Has Been Delete Successfully");
      return redirect()->back();
    }
}


