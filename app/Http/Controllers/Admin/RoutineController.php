<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sclass;
use App\Models\ResultType;
use App\Models\Routine;
use App\Models\Examroutine;
use yajra\Datatables\Datatables;
use Crypt;
use DB;
use Session;

class RoutineController extends Controller
{
    public function routineList()
    {
    	return view('admin.routine.routine-list');
    }

    public function routineForm()
    {
        $class = Sclass::where('status', 1)->get();
    	return view('admin.routine.routine-form', compact('class'));
    }

    public function routineStore(Request $request)
    {
       $this->validate($request, [
       	  'class_id' => 'not_in:0',
       	  'pdf' => 'required|mimes:doc,pdf,docx',
          'year' => 'required',
          'publish_date' => 'required'
       ]);

      try{
        $check = Routine::where('class_id', $request->class_id)->where('status', 1)->first();
   	    DB::beginTransaction();
          
		if ($check) {
		     Session::flash('error', "This class name already exists, please type another name.");
		     return redirect()->back();
		 } 
		$routine = new Routine();       
        $routine->title = $request->title;
        $routine->class_id = $request->class_id;

        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('routine-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $routine->pdf_file = $yearMonth . $file_path;
        }
        $routine->publish_date = date('Y-m-d', strtotime($request->get('publish_date')));
        $routine->year = $request->year;
        $routine->status = $request->status;
        $routine->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
       }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : R-101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
    }

    public function getRoutineList()
    {
    	// $routine = Routine::where('status', 1)->get();
    	$routine = Routine::join('class','class_routine.class_id', '=', 'class.id')
    	              ->get(['class_routine.*', 'class.name']);
 
    	return Datatables::of($routine)
            ->editColumn('sl_no', function ($routine) {
                return '';
            }) 
            ->editColumn('action', function ($routine) {
                $html = '';
                    $html = ' <a href="' . url('routine/edit/'. Crypt::encrypt($routine->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('routine/delete/' . Crypt::encrypt($routine->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('pdf_file', function($routine){
              
              if (!empty($routine->pdf_file)) {
                  return '<a href="/uploads/'.$routine->pdf_file.'" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-folder-open-o"></i> Open File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($routine){
              if ($routine->publish_date != '0000-00-00') {
                return date('Y-m-d', strtotime($routine->publish_date));
              }
            })
            ->addColumn('status', function ($routine) {
                if($routine->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Un Publish</label>";
               }
            })
            ->rawColumns(['status', 'action','pdf_file'])
            ->make(true);
    }

    public function routineEditView($id)
    {
        $routine = Routine::join('class','class_routine.class_id', '=', 'class.id')
    	              ->where('class_routine.id', Crypt::decrypt($id))->first(['class_routine.*', 'class.name']);

    	return view('admin.routine.routine-form-edit', compact('routine'));
        
    }

    public function routineEdit(Request $request)
    {
    
       if (!is_null($request->pdf_file)) {
		   	$this->validate($request, [
		      'pdf' => 'required|mimes:doc,pdf,docx'
		   ]);

        $this->validate($request, [
          'class_id' => 'not_in:0',
          'year' => 'required',
          'publish_date' => 'required'
       ]);

       }
       try{
   	    DB::beginTransaction();	
        $routine = Routine::find($request->id);
        $routine->title = $request->title;
        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('routine-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $routine->pdf_file = $yearMonth . $file_path;
        }
        $routine->publish_date = date('Y-m-d', strtotime($request->get('publish_date')));
        $routine->year = $request->year;
        $routine->status = $request->status;
        $routine->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
       }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : R-101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
    }

    public function Delete($id)
    {
      Routine::where('id', Crypt::decrypt($id))->delete();
	    Session::flash('success', "Data Has Been Deleted Successfully");
	    return redirect()->back(); 
    }

    public function examRoutineList()
    {
    	return view('admin.exam-routine.exam-routine-list');
    }

    public function examRoutineForm()
    {
    	$class = Sclass::where('status', 1)->get();
    	$result_type = ResultType::where('status', 1)->get();
    	return view('admin.exam-routine.exam-routine-form', compact('class', 'result_type'));
    }

    public function examRoutineStore(Request $request)
    {
    	$this->validate($request, [
       	  'class_id' => 'not_in:0',
       	  'result_type_id' => 'not_in:0',
       	  'pdf' => 'required|mimes:doc,pdf,docx',
          'year' => 'required',
          'publish_date' => 'required'
       ]);

      try{
        
   	    DB::beginTransaction();
           
		$exam_routine = new Examroutine();       
        $exam_routine->class_id = $request->class_id;
        $exam_routine->result_type_id = $request->result_type_id;

        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('routine-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $exam_routine->pdf_file = $yearMonth . $file_path;
        }
        $exam_routine->publish_date = (is_null($request->publish_date)) ? '' : date('Y-m-d', strtotime($request->get('publish_date')));
        $exam_routine->year = $request->year;
        $exam_routine->status = $request->status;
        $exam_routine->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
       }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : R-101)');
        dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
    }

    public function getExamRoutineList()
    {
    	$exam_routine = Examroutine::join('class','exam_routine.class_id', '=', 'class.id')
                        ->join('result_type', 'exam_routine.result_type_id', '=', 'result_type.id')
                        ->where('exam_routine.soft_delete', 1)
    	                ->get(['exam_routine.*', 'class.name as class_name', 'result_type.name as type']);

    	return Datatables::of($exam_routine)
            ->editColumn('sl_no', function ($exam_routine) {
                return '';
            }) 
            ->editColumn('action', function ($exam_routine) {
                $html = '';
                    $html = ' <a href="' . url('exam/exam-routine-edit/'. Crypt::encrypt($exam_routine->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('exam/exam-routine-delete/' . Crypt::encrypt($exam_routine->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('pdf_file', function($exam_routine){
              
              if (!empty($exam_routine->pdf_file)) {
                  return '<a href="/uploads/'.$exam_routine->pdf_file.'" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-folder-open-o"></i> Open File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($exam_routine){
              if ($exam_routine->publish_date) {
                return date('Y-m-d', strtotime($exam_routine->publish_date));
              }
            })
            ->addColumn('status', function ($exam_routine) {
                if($exam_routine->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Un Publish</label>";
               }
            })
            ->rawColumns(['status', 'action','pdf_file'])
            ->make(true);
    }

    public function examRoutineEditView($id)
    {
    	$class = Sclass::where('status', 1)->get();
    	$result_type = ResultType::where('status', 1)->get();
        $exam_routine = Examroutine::find(Crypt::decrypt($id));
       return view('admin.exam-routine.exam-routine-form-edit', compact('class', 'result_type', 'exam_routine'));
    }

    public function examRoutineUpdate(Request $request)
    {
    	$this->validate($request, [
       	  'class_id' => 'not_in:0',
       	  'result_type_id' => 'not_in:0',
          'year' => 'required',
          'publish_date' => 'required'
       ]);

    	if (!is_null($request->pdf)) {
    	$this->validate($request, [
       	  'pdf' => 'required|mimes:doc,pdf,docx'
        ]);
    	}

      try{
        
   	    DB::beginTransaction(); 
		$exam_routine = Examroutine::find($request->id);       
        $exam_routine->class_id = $request->class_id;
        $exam_routine->result_type_id = $request->result_type_id;

        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('routine-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $exam_routine->pdf_file = $yearMonth . $file_path;
        }
        $exam_routine->publish_date = (is_null($request->publish_date)) ? '' : date('Y-m-d', strtotime($request->get('publish_date')));
        $exam_routine->year = $request->year;
        $exam_routine->status = $request->status;
        $exam_routine->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
       }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : R-208)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
    }

    public function examRoutineDelete($id)
    {
        $result = Examroutine::find(Crypt::decrypt($id));
    	$result->status = 0;
    	$result->soft_delete = 0;
    	$result->save();

    	Session::flash('success', "Data Has Been Deleted Successfully");
        return redirect()->back(); 
    }
}
