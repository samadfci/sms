<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Sclass;
use App\Models\ResultType;
use App\Models\Result;
use yajra\Datatables\Datatables;
use Crypt;
use DB;
use Session;

class ResultController extends Controller
{
    public function resultList()
    {
    	return view('admin.result.result-list');
    }

    public function resultForm()
    {
    	$class = Sclass::where('status', 1)->get();
    	$result_type = ResultType::where('status', 1)->get();
    	return view('admin.result.result-form',compact('class','result_type'));
    }

    public function resultStore(Request $request)
    {
       $this->validate($request, [
       	  'class_id' => 'not_in:0',
       	  'result_type_id' => 'not_in:0',
       	  'pdf' => 'required|mimes:doc,pdf,docx,doc',
          'year' => 'required',
          'publish_date' => 'required'
       ]);

       try{
   	    DB::beginTransaction();
        $result = new Result();        
        $result->title = $request->title;
        $result->class_id = $request->class_id;
        $result->result_type_id = $request->result_type_id;

        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('result-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $result->result_pdf = $yearMonth . $file_path;
        }
        $result->publish_date = (is_null($request->publish_date)) ? '' : date('Y-m-d', strtotime($request->get('publish_date')));
        $result->year = $request->year;
        $result->status = $request->status;
        $result->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
       }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : Result-101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
    }

    public function getResultList()
    {
    	$result = Result::join('class','results.class_id', '=', 'class.id')
                            ->join('result_type', 'results.result_type_id', '=', 'result_type.id')
                            ->where('results.soft_delete', 1)
    	                    ->get(['results.*', 'class.name as class_name', 'result_type.name as type']);

    	return Datatables::of($result)
            ->editColumn('sl_no', function ($result) {
                return '';
            }) 
            ->editColumn('action', function ($result) {
                $html = '';
                    $html = ' <a href="' . url('result/edit/'. Crypt::encrypt($result->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('result/delete/' . Crypt::encrypt($result->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('result_pdf', function($result){
              
              if (!empty($result->result_pdf)) {
                  return '<a href="/uploads/'.$result->result_pdf.'" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-folder-open-o"></i> Open File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($result){
              if ($result->publish_date) {
                return date('Y-m-d', strtotime($result->publish_date));
              }
            })
            ->addColumn('status', function ($result) {
                if($result->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Un Publish</label>";
               }
            })
            ->rawColumns(['status', 'action','result_pdf'])
            ->make(true);
    }

    public function resultEditView($id)
    {
        $result = Result::find(Crypt::decrypt($id));
        $class  = Sclass::where('status', 1)->get(); 
    	$result_type = ResultType::where('status', 1)->get();

    	return view('admin.result.result-form-edit',compact('result_type','result','class')); 
    }

    public function resultUpdate(Request $request)
    {
        $this->validate($request, [
       	  'class_id' => 'not_in:0',
       	  'result_type_id' => 'not_in:0',
          'year' => 'required',
          'publish_date' => 'required'
       ]);

        if (!is_null($request->pdf)) {
        $this->validate($request, [
          'pdf' => 'required|mimes:doc,pdf,docx'
       ]);
      }

       try{
   	    DB::beginTransaction();
        $result = Result::find($request->id);        
        $result->title = $request->title;
        $result->class_id = $request->class_id;
        $result->result_type_id = $request->result_type_id;

        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('result-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $result->result_pdf = $yearMonth . $file_path;
        }
        $result->publish_date = (is_null($request->publish_date)) ? '' : date('Y-m-d', strtotime($request->get('publish_date')));
        $result->year = $request->year;
        $result->status = $request->status;
        $result->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back();
       }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : Result-101)');
        dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
    }

    public function Delete($id)
    {
      dd(1);
    	$result = Result::find(Crypt::decrypt($id));
    	$result->status = 0;
    	$result->soft_delete = 0;
    	$result->save();

    	Session::flash('success', "Data Has Been Deleted Successfully");
      return redirect()->back(); 
    }
}
