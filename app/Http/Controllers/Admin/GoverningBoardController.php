<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use yajra\Datatables\Datatables;
use App\Models\Designation;
use App\Models\Committee;
use Crypt;
use DB;
use Session;
use Carbon\Carbon;

class GoverningBoardController extends Controller
{
    public function designationList()
    {

    	return view('admin.governing-board.designationList');
    }

    public function getDesignationList()
    {
        $designation = Designation::get();
        return Datatables::of($designation)
            ->editColumn('sl_no', function ($designation) {
                return '';
            }) 
            ->editColumn('action', function ($designation) {
                $html = '';
                    $html = ' <a href="' . url('designation/edit/'. Crypt::encrypt($designation->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                return $html;
            })
            ->addColumn('status', function ($designation) {
                if($designation->status == 1){
                   return "<label class='btn btn-success btn-sm'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>In Active</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function designationForm()
    {
         return view('admin.governing-board.designation-form');
    }

    public function designationStore(Request $request)
    {
    	 $this->validate($request,[
                'name' => 'required'
            ]);
            
            try{

            DB::beginTransaction();
            if (!empty($request->id)) {
              $designation = Designation::find($request->id);
            }else{
              $designation = Designation::where('name', $request->name)->first();
              if ($designation) {
                Session::flash('error', "This name already exists !");
                return redirect()->back();  
              }else{
                $designation = new Designation();
                
              }
            }

            $designation->name = $request->name;
            $designation->status = $request->status;
            $designation->save();
            DB::commit();
            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();

           }catch (Exception $e) {
                DB::rollback();
                Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
                //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
                return redirect()->back();
            }
    }

    public function editForm($id)
    {
    	$designation = Designation::find(Crypt::decrypt($id));
    	return view('admin.governing-board.designation-form-edit',compact('designation'));
    }

    public function committeeList()
    {
        return view('admin.governing-board.committeeList');
    }

    public function committeeForm()
    {
        $designation = Designation::where('status', 1)->get(['name','id']);
        return view('admin.governing-board.committee-form',compact('designation'));
    }

    public function committeeStore(Request $request)
    {
            $this->validate($request,[
                'name' => 'required',
                'designation_id' => 'required|not_in:0',
                'phone' => 'required',
                'join_date' => 'required' 
            ]);

         try{
            DB::beginTransaction();
           if ($request->id) {
               $committee = Committee::find($request->id);
           }else{
              $committee = new Committee();
           }

           $committee->designation_id = $request->designation_id;
           $committee->name = $request->name;
           $committee->qualification = $request->qualification;
           $committee->email = $request->email;
           $committee->phone = $request->phone;
           $committee->gender = $request->gender;
           $committee->address = $request->address;
           $committee->join_date = date('Y-m-d', strtotime($request->get('join_date')));
           if (!empty($request->rejoined_date)) {
            $committee->rejoined_date = date('Y-m-d', strtotime($request->get('rejoined_date')));
           }
           if ($request->hasFile('photo')) {
               $yearMonth = date("Y") . "/" . date("m") . "/";
               $path = 'uploads/' . $yearMonth;
               if (!file_exists($path)) {
                   mkdir($path, 0777, true);
               }
               $_file_path = $request->file('photo');
               $file_path = trim(uniqid('about-' . $request->phone . '-', true) . $_file_path->getClientOriginalName());
               $_file_path->move($path, $file_path);
               $committee->photo = $yearMonth . $file_path;
            }
           $committee->status = $request->status;
           $committee->message = $request->message;
           $committee->save();

            DB::commit();
            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();

         }catch (Exception $e){
            DB::rollback();
            Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
            //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
            return redirect()->back();
         }
    }

    public function getCommitteeList()
    {
        $data = DB::table('committees')
            ->join('designations', 'committees.designation_id', '=', 'designations.id')
            ->where('committees.is_archive', 1)
            ->where('committees.status', 1)
            ->select('committees.*', 'designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($notice) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a href="' . url('committee/committee-open/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                    $html .= ' <a href="' . url('committee/edit/'. Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('committee/delete/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->addColumn('status', function ($data) {
                if($data->status == 1){
                   return "<label class='btn btn-success btn-xs'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-xs'>Inactive</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }

    public function committeeEditForm($id)
    {
      $designation = Designation::where('status', 1)->get(['name','id']);
      $committee = Committee::find(Crypt::decrypt($id));
      $viewMode = 'on';
      
      return view('admin.governing-board.committee-form-edit',compact('designation','committee','viewMode'));
    }

    public function committeeView($id)
    {
      $designation = Designation::where('status', 1)->get(['name','id']);
      $committee = Committee::find(Crypt::decrypt($id));
      $viewMode = 'off';
      
      return view('admin.governing-board.committee-form-edit',compact('designation','committee','viewMode'));
    }

    public function committeeDelete($id)
    {
      $committee = Committee::find(Crypt::decrypt($id));
      $committee->is_archive = 0;
      $committee->status = 2;
      $committee->save();

      Session::flash('success', "Data Has Been Delete Successfully");
      return redirect()->back();
    }

    public function chairmanMessage()
    {

      $message = Committee::join('messages', 'messages.message_type', '=', 'committees.designation_id')
                   ->where('committees.status', 1)
                   ->where('messages.id', 1)
                   ->where('messages.status', 1)
                   ->first(['committees.designation_id','messages.*']);


      return view('admin.governing-board.chairman-message',compact('message'));
    }

    public function updateMessage(Request $request)
    {
      
      $this->validate($request,[
                'message_body' => 'required',
                'message_type' => 'required'
            ]);
      $message = DB::table('messages')
                     ->where('id', $request->id)
                     ->where('message_type', $request->message_type)
                     ->update([
                        'message_body' => $request->message_body,
                        'message_type' => $request->message_type,
                        'status'       => $request->status,
                    ]);

      Session::flash('success', "Data Has Been Update Successfully");
      return redirect()->back();               
    }

    public function exCommitteelist()
    {
      return view('admin.governing-board.ex-committee-list');
    }

    public function getExCommitteeList()
    {
      $data = DB::table('committees')
            ->join('designations', 'committees.designation_id', '=', 'designations.id')
            ->where('designations.status', 1)
            ->where('committees.is_archive', 1)
            ->where('committees.status', 2)
            ->select('committees.*', 'designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($notice) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a href="' . url('committee/committee-open/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-primary"><i class="fa fa-folder-open-o"></i> Open</a>';
                    $html .= ' <a href="' . url('committee/edit/'. Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('committee/delete/' . Crypt::encrypt($data->id)) . '" class="btn btn-xs btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->addColumn('status', function ($data) {
                if($data->status == 1){
                   return "<label class='btn btn-success btn-xs'>Active</label>";
               }else{
                   return "<label class='btn btn-danger btn-xs'>Inactive</label>";
               }
            })
            ->rawColumns(['status', 'action'])
            ->make(true);
    }
}
