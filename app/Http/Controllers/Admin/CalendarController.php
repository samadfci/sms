<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Calendar;
use yajra\Datatables\Datatables;
use Crypt;
use DB;
use Session;

class CalendarController extends Controller
{
    public function calendarList()
    {
    	return view('admin.calendar.calendar-list');
    }

    public function getCalendarList(){
       
       $calendar = Calendar::where('is_archive', 0)->get();   

       return Datatables::of($calendar)
            ->editColumn('sl_no', function ($calendar) {
                return '';
            }) 
            ->editColumn('action', function ($calendar) {
                $html = '';
                    $html = ' <a href="' . url('calender/calender-edit/'. Crypt::encrypt($calendar->id)) . '" class="btn btn-sm btn-info"> <i class="fa fa-edit"></i> Edit</a> ';
                    $html .= ' <a href="' . url('calender/calender-delete/' . Crypt::encrypt($calendar->id)) . '" class="btn btn-sm btn-danger" onclick="return confirm(\'Are you sure you want to delete this?\');"> <i class="fa fa-times"></i> Delete</a> ';
                return $html;
            })->addColumn('pdf_file', function($calendar){
              
              if (!empty($calendar->pdf_file)) {
                  return '<a href="/uploads/'.$calendar->pdf_file.'" class="btn btn-xs btn-primary" target="_blank"><i class="fa fa-folder-open-o"></i> Open File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($calendar){
              if ($calendar->publish_date) {
                return date('Y-m-d', strtotime($calendar->publish_date));
              }
            })
            ->addColumn('status', function ($calendar) {
                if($calendar->status == 1){
                   return "<label class='btn btn-success btn-sm'>Publish</label>";
               }else{
                   return "<label class='btn btn-danger btn-sm'>Unpublish</label>";
               }
            })
            ->rawColumns(['status', 'action','pdf_file'])
            ->make(true);
    }

    public function calendarForm()
    {

    	return view('admin.calendar.calendar-form');
    }

    public function calendarStore(Request $request)
    {
    	$this->validate($request, [
       	  'pdf' => 'required|mimes:doc,pdf,docx,doc',
       	  'year' => 'required',
       	  'publish_date' => 'required'
       ]);

       try{
        $calendar = new Calendar();
        $calendar->title = $request->title;  
        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('calendar-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $calendar->pdf_file = $yearMonth . $file_path;
        }
        $calendar->publish_date = (is_null($request->publish_date)) ? '' : date('Y-m-d', strtotime($request->get('publish_date')));
        $calendar->year = $request->year;
        $calendar->status = $request->status;
        $calendar->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back(); 
        }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : Result-101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
        
    }

    public function calendarEditView($id)
    {
    	$calendar = Calendar::find(Crypt::decrypt($id));
    	return view('admin.calendar.calendar-form-edit', compact('calendar'));
    }

    public function calendarUpdate(Request $request)
    {

       $this->validate($request, [
       	  'year' => 'required',
       	  'publish_date' => 'required'
       ]);

       try{
        $calendar = Calendar::find($request->id);
        $calendar->title = $request->title;  
        if ($request->hasFile('pdf')) {
       $yearMonth = date("Y") . "/" . date("m") . "/";
       $path = 'uploads/' . $yearMonth;
       if (!file_exists($path)) {
           mkdir($path, 0777, true);
       }
       $_file_path = $request->file('pdf');
       $file_path = trim(uniqid('calendar-' . date('i') . '-', true) . $_file_path->getClientOriginalName());
       $_file_path->move($path, $file_path);
       $calendar->pdf_file = $yearMonth . $file_path;
        }
        $calendar->publish_date = (is_null($request->publish_date)) ? '' : date('Y-m-d', strtotime($request->get('publish_date')));
        $calendar->year = $request->year;
        $calendar->status = $request->status;
        $calendar->save();

        DB::commit();
        Session::flash('success', "Data Has Been Stored Successfully");
        return redirect()->back(); 
        }catch(\Exception $e){

        DB::rollback();
        Session::flash('error', 'Some thing went wrong (ErrorCode : Result-101)');
        // dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
        return redirect()->back();   
       }
    }

    public function calendarDelete($id)
    {
    	$calendar = Calendar::find(Crypt::decrypt($id));
    	$calendar->status = 0;
      $calendar->is_archive = 1;
    	$calendar->save();

    	Session::flash('success', "Data Has Been Deleted Successfully");
        return redirect()->back(); 
    }
}
