<?php

    namespace App\Http\Controllers\Admin;

    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Models\About;
    use DB;
    use Session;

    class AboutController extends Controller
    {
        public function applicationForm()
        {
            
            $about = About::where('status', 1)->where('id', 1)->first();
        	return view('admin.about.about',['about'=> $about]);
        }

        public function applicationStore(Request $request){
            
            $this->validate($request,[
                'title' => 'required',
                'description' => 'required',
                'photo' => 'mimes:jpeg,jpg,png,gif' 

            ]);

            try{

            DB::beginTransaction();
            $udateAbout = About::find($request->id);
            $udateAbout->title = $request->title;
            $udateAbout->description = $request->description;

                if ($request->hasFile('photo')) {

               $yearMonth = date("Y") . "/" . date("m") . "/";
               $path = 'uploads/' . $yearMonth;
               if (!file_exists($path)) {
                   mkdir($path, 0777, true);
               }
               $_file_path = $request->file('photo');
               $file_path = trim(uniqid('about-' . $request->id . '-', true) . $_file_path->getClientOriginalName());
               $_file_path->move($path, $file_path);
               $udateAbout->photo = $yearMonth . $file_path;
            }
            $udateAbout->save();

            DB::commit();
            Session::flash('success', "Data Has Been Stored Successfully");
            return redirect()->back();

           }catch (Exception $e) {
                DB::rollback();
                Session::flash('error', 'Some thing went wrong (ErrorCode : Item101)');
                //dd($e->getMessage(), $e->getLine(), $e->getCode(), $e->getFile());
                return redirect()->back();
            }

        }
    }


