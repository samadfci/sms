<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Routine;
use App\Models\Examroutine;
use App\Models\Result;
use App\Models\Committee;
use App\Models\Teacher;
use App\Models\Notice;
use App\Models\Event;
use App\Models\Gallery;
use App\Models\Calendar;
use yajra\Datatables\Datatables;
use Crypt;
use DB;
use Carbon\Carbon;

class PageController extends Controller
{
    public function aboutUs()
    {
    	$about = About::find(1)->first();
    	return view('frontEnd.pages.about', compact('about'));
    }

    public function classRoutine()
    {
    	return view('frontEnd.pages.class-routine');
    }

    public function getClassRoutine()
    {
    	$routine = Routine::join('class','class_routine.class_id', '=', 'class.id')
    	              ->where('class_routine.status', 1)
    	              ->orderBy('class_routine.id', 'DESC')
    	              ->get(['class_routine.*', 'class.name']);
 
    	return Datatables::of($routine)
            ->editColumn('sl_no', function ($routine) {
                return '';
            }) 
            ->editColumn('action', function ($routine) {
                if (!empty($routine->pdf_file)) {
                  return '<a href="/uploads/'.$routine->pdf_file.'" class="btn btn-sm btn-info" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Download File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($routine){
              if ($routine->publish_date != '0000-00-00') {
                return date('Y-m-d', strtotime($routine->publish_date));
              }
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function examRoutine()
    {
    	return view('frontEnd.pages.exam-routine');
    }

    public function getExamRoutine()
    {
    	$exam_routine = Examroutine::join('class','exam_routine.class_id', '=', 'class.id')
                        ->join('result_type', 'exam_routine.result_type_id', '=', 'result_type.id')
                        ->where('exam_routine.status', 1)
                        ->where('exam_routine.soft_delete', 1)
                        ->orderBy('exam_routine.id', 'DESC')
    	                ->get(['exam_routine.*', 'class.name as class_name', 'result_type.name as type']);

    	return Datatables::of($exam_routine)
            ->editColumn('sl_no', function ($exam_routine) {
                return '';
            }) 
            ->editColumn('action', function ($exam_routine) {
               if (!empty($exam_routine->pdf_file)) {
                  return '<a href="/uploads/'.$exam_routine->pdf_file.'" class="btn btn-sm btn-info" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Download File';
               }else{
                 return '';
               }   
            })->addColumn('publish_date', function($exam_routine){
              if ($exam_routine->publish_date) {
                return date('Y-m-d', strtotime($exam_routine->publish_date));
              }
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function examResult()
    {
    	return view('frontEnd.pages.exam-result');
    }

    public function getExamResult()
    {
    	$result = Result::join('class','results.class_id', '=', 'class.id')
                            ->join('result_type', 'results.result_type_id', '=', 'result_type.id')
                            ->where('results.soft_delete', 1)
                            ->where('results.status', 1)
                            ->orderBy('results.id', 'DESC')
    	                    ->get(['results.*', 'class.name as class_name', 'result_type.name as type']);

    	return Datatables::of($result)
            ->editColumn('sl_no', function ($result) {
                return '';
            }) 
            ->editColumn('action', function ($result) {
                if (!empty($result->result_pdf)) {
                  return '<a href="/uploads/'.$result->result_pdf.'" class="btn btn-sm btn-info" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Download File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($result){
              if ($result->publish_date) {
                return date('Y-m-d', strtotime($result->publish_date));
              }
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function governingBody()
    {
    	return view('frontEnd.pages.governing-body');
    }

    public function getGoverningBody()
    {
    	$data = DB::table('committees')
            ->join('designations', 'committees.designation_id', '=', 'designations.id')
            ->where('committees.is_archive', 1)
            ->where('committees.status', 1)
            ->select('committees.*', 'designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($data) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a data-id="'.$data->id.'" class="btn btn-xs btn-primary" id="profile" data-url="' . url('profile/'. $data->id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-folder-open"></i> Open</a>';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function profile($id)
    {
        $profile = DB::table('committees')
            ->join('designations', 'committees.designation_id', '=', 'designations.id')
            ->where('committees.is_archive', 1)
            ->where('committees.status', 1)
            ->where('committees.id', $id)
            ->first(['committees.*', 'designations.name as designation']);

    	return json_encode($profile);	
    }

    public function teachersStaff()
    {
       return view('frontEnd.pages.teacher-staff');
    }

    public function getTeachersStaff()
    {
       $data = DB::table('teachers')
            ->join('te_designations', 'teachers.designation_id', '=', 'te_designations.id')
            ->where('teachers.soft_delete', 1)
            ->where('teachers.status', 1)
            ->select('teachers.*', 'te_designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($data) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a data-id="'.$data->id.'" class="btn btn-xs btn-primary" id="profile" data-url="' . url('teacher-profile/'. $data->id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-folder-open"></i> Open</a>';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function teacherProfile($id)
    {
        $data = DB::table('teachers')
            ->join('te_designations', 'teachers.designation_id', '=', 'te_designations.id')
            ->where('teachers.soft_delete', 1)
            ->where('teachers.status', 1)
            ->where('teachers.id', $id)
            ->first(['teachers.*', 'te_designations.name as designation']);

            return json_encode($data);
    }

    public function exGoverningBody()
    {
       return view('frontEnd.pages.ex-governing-body'); 
    }

    public function getExGoverningBody()
    {
        $data = DB::table('committees')
            ->join('designations', 'committees.designation_id', '=', 'designations.id')
            ->where('committees.is_archive', 1)
            ->where('committees.status', 2)
            ->select('committees.*', 'designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($data) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a data-id="'.$data->id.'" class="btn btn-xs btn-primary" id="profile" data-url="' . url('ex-profile/'. $data->id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-folder-open"></i> Open</a>';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function exProfile($id)
    {
         $profile = DB::table('committees')
            ->join('designations', 'committees.designation_id', '=', 'designations.id')
            ->where('committees.is_archive', 1)
            ->where('committees.status', 2)
            ->where('committees.id', $id)
            ->first(['committees.*', 'designations.name as designation']);

        return json_encode($profile);
    }

    public function ExTeachersStaff()
    {
        return view('frontEnd.pages.ex-teacher-staff');
    }

    public function getExTeachersStaff()
    {
        $data = DB::table('teachers')
            ->join('te_designations', 'teachers.designation_id', '=', 'te_designations.id')
            ->where('teachers.soft_delete', 1)
            ->where('teachers.status', 2)
            ->select('teachers.*', 'te_designations.name as designation')
            ->get();

        return Datatables::of($data)
            ->editColumn('sl_no', function ($data) {
                return '';
            }) 
            ->editColumn('action', function ($data) {
                $html = '';
                    $html = '<a data-id="'.$data->id.'" class="btn btn-xs btn-primary" id="profile" data-url="' . url('ex-teacher-profile/'. $data->id) . '" data-toggle="modal" data-target="#myModal"><i class="fa fa-folder-open"></i> Open</a>';
                return $html;
            })
            ->addColumn('duration', function($data){
              if (!is_null($data->join_date)) {
                $start =  Carbon::parse($data->join_date);
              }
              if (is_null($data->rejoined_date)) {
                $end = Carbon::now();
              }else{
                $end = Carbon::parse($data->rejoined_date);
              }
               return $start->diff($end)->format('%y years, %m months and %d days');
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function ExTeacherProfile($id)
    {
        $data = DB::table('teachers')
            ->join('te_designations', 'teachers.designation_id', '=', 'te_designations.id')
            ->where('teachers.soft_delete', 1)
            ->where('teachers.status', 2)
            ->where('teachers.id', $id)
            ->first(['teachers.*', 'te_designations.name as designation']);

            return json_encode($data);
    }

    public function getChairmanMessage()
    {
        $message = Committee::join('messages', 'messages.message_type', '=', 'committees.designation_id')
                   ->where('committees.status', 1)
                   ->where('messages.id', 1)
                   ->where('messages.status', 1)
                   ->first(['committees.designation_id','messages.*', 'committees.photo']);
                   
        return view('frontEnd.pages.chairman-message',compact('message'));
    }

    public function getPrincipalMessage()
    {
        $message = Teacher::join('messages', 'messages.message_type', '=', 'teachers.designation_id')
           ->where('teachers.status', 1)
           ->where('messages.status', 1)
           ->where('messages.id', 2)
           ->first(['teachers.designation_id','messages.*','teachers.photo']);

           return view('frontEnd.pages.principal-message',compact('message'));
    }

    public function allNotice()
    {
        return view('frontEnd.pages.all-notice');
    }

    public function getAllNotice()
    {
        $notice = Notice::where(array('is_archive' => '1', 'status' => '1'))->orderBy('id', 'desc')
            ->get(['id','title','pdf','publish_date','status']);

        return Datatables::of($notice)
            ->editColumn('sl_no', function ($notice) {
                return '';
            }) 
            ->editColumn('action', function ($notice) {
                $html = '';
                $html = '<a href="' . url('single-notice-open/' . Crypt::encrypt($notice->id)) . '" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-folder-open"></i> Open</a>';
                if (!empty($notice->pdf)) {
                $html .= ' <a href="/uploads/'.$notice->pdf.'" class="btn btn-sm btn-info" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Download File</a>';
                    }
                return $html;
            })->addColumn('publish_date', function($notice){
              if ($notice->publish_date != '0000-00-00') {
                return date('Y-m-d', strtotime($notice->publish_date));
              }
            })
            ->rawColumns(['action'])
            ->make(true);    
    }

    public function singleView($id)
    {
        $notice = Notice::find(Crypt::decrypt($id));
        return view('frontEnd.pages.single-notice-open', compact('notice'));
    }

    public function allEvent()
    {
        return view('frontEnd.pages.all-event');
    }

    public function getAllEvent()
    {
        $event = Event::where(array('soft_delete' => '0', 'status' => '1'))->orderBy('id', 'desc')->get();

        return Datatables::of($event)
            ->editColumn('sl_no', function ($event) {
                return '';
            }) 
            ->editColumn('action', function ($event) {
                $html = '';
                    $html = '<a href="' . url('single-event-open/' . Crypt::encrypt($event->id)) . '" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-folder-open"></i> Open</a>';
                return $html;
            })->addColumn('publish_date', function($event){
              if (!is_null($event->publish_date)) {
                return date('Y-m-d', strtotime($event->publish_date));
              }
              return '';
            })->addColumn('title',function($event){
                return substr($event->title, 0, 60);
            })
            ->rawColumns([ 'action'])
            ->make(true);
    }

    public function singleEvent($id)
    {
        $event = Event::find(Crypt::decrypt($id));
        return view('frontEnd.pages.single-event-open', compact('event'));
    }

    public function Contact()
    {
        $contact = DB::table('contact')->where('id', 1)->first();
        return view('frontEnd.pages.contact', compact('contact'));
    }

    public function allGallary()
    {
        $gallary = Gallery::where('status', 1)->orderBy('id', 'asc')->get();
        $col = count($gallary); 
        
        return view('frontEnd.pages.all-gallary', compact('gallary', 'col'));
    }

    public function showImg($id)
    {
        $data = Gallery::find($id);
        return json_encode($data);
    }

    public function Calendar()
    {
        return view('frontEnd.pages.calendar');
    }

    public function academicCalendarList()
    {
       $calendar = Calendar::where('is_archive', 0)->where('status', 1)->get();

        return Datatables::of($calendar)
            ->editColumn('sl_no', function ($calendar) {
                return '';
            }) 
            ->editColumn('action', function ($calendar) {
                if (!empty($calendar->pdf_file)) {
                  return '<a href="/uploads/'.$calendar->pdf_file.'" class="btn btn-sm btn-info" target="_blank"><i class="fa fa-download" aria-hidden="true"></i> Download File</a>';
               }else{
                 return '';
               }
            })->addColumn('publish_date', function($calendar){
              if ($calendar->publish_date) {
                return date('Y-m-d', strtotime($calendar->publish_date));
              }
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
