<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Notice;
use App\Models\Committee;
use App\Models\Teacher;
use App\Models\TeDesignation;
use App\Models\Link;
use App\Models\Event;
use App\Models\Setting;
use App\Models\Slider;
use App\Models\Gallery;
use DB;
use App;


class FrontendController extends Controller
{

	public function index(){
        

        //about data
		$about = About::find(1)->first();
		//notice data
        $notice = Notice::Where('is_archive',1)->orderBy('id', 'DESC')->limit(10)->get();
         //event 
         $events = Event::Where('status', 1)->orderBy('id', 'DESC')->limit(3)->get();

         //setting ...
         $setting = Setting::find('1');

         //slider ....
         $slider = Slider::where('status', 1)->orderBy('id', 'DESC')->limit(5)->get();
         // dd(count($slider));

         //gallery ...
         $gallery = Gallery::where('status', 1)->orderBy('id', 'DESC')->limit(16)->get();         
         
		return view('frontend.pages.home',['about' => $about, 'notice' => $notice,
     'events' => $events, 'setting' => $setting, 'slider' => $slider, 'gallery' => $gallery,]);
	}
    
}
