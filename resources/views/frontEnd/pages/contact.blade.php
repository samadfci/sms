@extends('frontEnd.layouts')
@section('style')
<style type="text/css">
	.welcome-page-title{
		text-align: center;
		padding-top: 15px;
		font-size: 24px;
		margin-bottom: 20px;
		line-height: 1.5;
		color: #000;
		text-transform: uppercase;
		position: relative;
		font-family: "Roboto Slab", serif;
	    font-weight: 400;
	    margin: 0 0 20px 0;
	}
	.single-notice{
		background: #f5f5f5 none repeat scroll 0 0;
	    border : 1px solid #cccccc !important;
	}
	.contact tr td{
		padding: 5px;
	}
</style>
@stop
@section('pages')
<div class="page-title"><i class="fa fa-home"></i> Contact</div><br>
<div class="welcome-post single-notice">
     <table class="contact">
        <tr>
          <td><i class="fa fa-map-marker font-color"></i></td>
          <td style="padding-left: 10px;">{{ (!is_null($contact->address) ? $contact->address : '') }}</td>
        </tr>

        <tr>
          <td><i class="fa fa-phone font-color"></i></td>
          <td style="padding-left: 10px;">{{ (!is_null($contact->phone) ? $contact->phone : '') }}</td>
        </tr>
        <tr>
          <td><i class="fa fa-envelope font-color"></i></td>
          <td style="padding-left: 10px;">{{ (!is_null($contact->email) ? $contact->email : '') }}</td>
        </tr>
      </table>
</div> 
@stop