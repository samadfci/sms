@extends('frontEnd.layouts')
@section('style')
<style type="text/css">
  .welcome-page-title{
    text-align: center;
    padding-top: 15px;
    font-size: 24px;
    margin-bottom: 20px;
    line-height: 1.5;
    color: #000;
    text-transform: uppercase;
    position: relative;
    font-family: "Roboto Slab", serif;
      font-weight: 400;
      margin: 0 0 20px 0;
  }
  .message{
    font-size: 16px;
    padding: 15px 20px;
  }
  .message-div{
    background: #f5f5f5 none repeat scroll 0 0;
    border: 1px solid #cccccc !important;
  }
  .message img{
    /*border: 4px solid #666 !important;*/
    border: 5px solid #fff;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.12), 0 2px 4px 0 rgba(0,0,0,0.08);
  }
</style>
@stop
@section('pages')
<div class="page-title"><i class="fa fa-home"></i> Chairman Message</div><br>
<div class="message message-div"> 
  <br>
  @if(!is_null($message))
  <img src="{{ URL::to('uploads/'.$message->photo) }}" width="180px" height="180px"><br><br><br>
   {!! $message->message_body !!}
  @endif
</div>
<br><br> 
@stop