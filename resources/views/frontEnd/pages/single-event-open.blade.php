
@extends('frontEnd.layouts')
@section('style')
<style type="text/css">
	.welcome-page-title{
		text-align: center;
		padding-top: 15px;
		font-size: 24px;
		margin-bottom: 20px;
		line-height: 1.5;
		color: #000;
		text-transform: uppercase;
		position: relative;
		font-family: "Roboto Slab", serif;
	    font-weight: 400;
	    margin: 0 0 20px 0;
	}
	.single-notice{
		background: #f5f5f5 none repeat scroll 0 0;
	    border : 1px solid #cccccc !important;
	}
	.single-notice img{
    /*border: 4px solid #666 !important;*/
    border: 5px solid #fff;
    
  }
</style>
@stop
@section('pages')
	<div class="page-title"><i class="fa fa-home"></i> Event </div><br>
<div class="welcome-post single-notice">
  <img src="{{ URL::to('uploads/'.$event->photo) }}" width="100%" height="320px">
  <div class="welcome-page-title">{{ $event->title }}</div>
  
  @if(!is_null($event->details))	
  {!! $event->details !!}
  @endif  
</div> <br>       
@stop
