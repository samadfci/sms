@extends('frontEnd.layouts')
@section('style')
<style type="text/css">
	.welcome-page-title{
		text-align: center;
		padding-top: 15px;
		font-size: 24px;
		margin-bottom: 20px;
		line-height: 1.5;
		color: #000;
		text-transform: uppercase;
		position: relative;
		font-family: "Roboto Slab", serif;
	    font-weight: 400;
	    margin: 0 0 20px 0;
	}
	.welcome-post{
		font-size: 16px;
	}
	.table>tbody>tr>td{
		border: none;
	}
	.routine{
      	background: #f5f5f5 none repeat scroll 0 0;
	      border : 1px solid #cccccc !important;
      }
      .modal-body{
      	padding: 0px 15px;
      }

</style>
<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css"> -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontEnd/js/gallery.css') }}">
@stop
@section('pages')
<div class="page-title"><i class="fa fa-home"></i> Gallary</div><br>
<div class="welcome-post routine">
  <div class="welcome-page-title"></div> 
        <div class="row">
        	<table id="example" class="table table-striped table-bordered" style="width:100%" >
        		<thead class="hidden">
        			<tr>
        				<td></td>
        			</tr>
        		</thead>
            <tbody>
        	<?php for ($i=0, $j=0; $i <$col ; $i++) { ?>
        		<?php if($i % 3 == 0) { $j = 1;}else{ $j++;} ?>
        		@if($j == 1)
        	<tr>
        		<td>
        		@endif
        			<div class="col-md-4">
        				<a id="profile" data-url="{{ url('show-img/'. $gallary[$i]->id) }}" data-id="'.$gallary[$i]->id.'" data-toggle="modal" data-target="#myModal">
        					<img src="{{ url('uploads/'.$gallary[$i]->photo) }}" alt="{{ $gallary[$i]->title }}" style="height: 180px" width="100%" class="img-responsive">
        				</a>
        			</div>        		   
        	@if($j == 3)
        	</td>
        	</tr>
        		@elseif($col - $i == 1)
        	</tr>
        		@endif

        <?php } ?>
        </tbody>
       </table>
        </div>
</div> 
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
<!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body">
        <div class="row">
            <img src="{{URL::to('/uploads/profile.jpg')}}" class="profile-img" alt="Preview Image" class="img-responsive" width="100%" height="420px" />
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
@stop

@section('js')
    <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    <script language="javascript">
        $(document).ready(function() {
		    $('#example').DataTable( {
		        "order": [[ 0, "desc" ]],
		        "searching": false,
		         "bLengthChange": false
		    } );    
		} );

		$(document).ready(function(){
            $(document).on('click', '#profile', function(e){

                e.preventDefault();
                var url = $(this).data('url');
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                    success: function (data) {

                        console.log(data);
                        var location = '';
                        if(data.photo != null) {
                            var img = "uploads/" + data.photo;
                            $(".modal-body .profile-img").attr('src', "{{asset('')}}" + img);
                        }

                        if(data.title != null) {
                            $(".modal-title").html(data.title);
                        }
  

                    }
                });

            });

        });
    </script>

@stop