@extends('frontEnd.layouts')
@section('style')
<style type="text/css">
	.welcome-page-title{
		text-align: center;
		padding-top: 15px;
		font-size: 24px;
		margin-bottom: 20px;
		line-height: 1.5;
		color: #000;
		text-transform: uppercase;
		position: relative;
		font-family: "Roboto Slab", serif;
	    font-weight: 400;
	    margin: 0 0 20px 0;
	}
	.welcome-post{
		font-size: 16px;
	}
</style>
@stop
@section('pages')
<div class="page-title"><i class="fa fa-home"></i> About Us</div><br>
<img src="{{ URL::to('uploads/'.$about->photo) }}" width="100%" height="320px">
<div class="welcome-post bg-div">
  <div class="welcome-page-title">{{ $about->title }}</div>	
  {!! $about->description !!}
</div> 
@stop