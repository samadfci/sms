@extends('frontEnd.layouts')
@section('style')
   <style type="text/css">
   	      .routine{
   	      	background: #f5f5f5 none repeat scroll 0 0;
			border : 1px solid #cccccc !important;
   	      }
   	      #notices tr > th{
   	      	background-color: #683091;
   	      	color: #fff;
   	      	font-size: 16px;
   	      }
          .modal-header-success {
            color:#fff;
            padding:9px 15px;
            border-bottom:1px solid #eee;
            background-color: #5cb85c;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
             border-top-left-radius: 5px;
             border-top-right-radius: 5px;
        }
        #single-view > tbody>tr>td{
            padding: 0px 8px !important;
            font-size: 16px;
          }
   </style>
@stop
@section('pages')
<div class="page-title"><i class="fa fa-home"></i> Governing Body</div><br>
<div class="welcome-post routine">
      <div class="table-responsive">
        <table id="notices" class="table table-striped resultTable display table-bordered" role="grid">
        <thead>
        <tr>
            <th>#</th>
            <th>Name</th>
            <th>Designation</th>
            <th>Duration</th>
            <th>Phone</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        	
        </tbody>
       </table>
      </div>
 </div>

 <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header modal-header-success">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><i class="fa fa-user"></i> <strong class="title">Profile</strong></h4>
      </div>
      <div class="modal-body">
        <div>
            <img src="{{URL::to('/uploads/profile.jpg')}}" class="profile-img" alt="Preview Image" width="180px" height="180px" />
        </div>
        <br>
        <table id="single-view">
            <tr>
                <td style="width: 20%"><label>Full Name :</label></td>
                <td class="name"></td>
            </tr>
            <tr>
                <td style="width: 20%"><label>Designation :</label></td>
                <td class="designation"></td>
            </tr>
            <tr>
                <td style="width: 20%"><label>Qualification :</label></td>
                <td class="qualification"></td>
            </tr>
            
            <tr>
                <td style="width: 20%"><label>Email :</label></td>
                <td class="email"></td>
            </tr>
            <tr>
                <td style="width: 20%"><label>Phone No :</label></td>
                <td class="phone"></td>
            </tr>
            <tr>
                <td style="width: 20%"><label>Gender :</label></td>
                <td class="gender"></td>
            </tr>
            <tr>
                <td style="width: 20%"><label>Address :</label></td>
                <td class="address"></td>
            </tr>
            <tr>
                <td style="width: 20%"><label>Join Date :</label></td>
                <td class="join_date"></td>
            </tr>
            <tr>
                <td style="width: 20%"><label>Message :</label></td>
                <td class="message"></td>
            </tr>
        </table>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>     
@stop

@section('js')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    <script language="javascript">
        $(document).ready(function () {
            $(function () {
                var t = $('#notices').DataTable({
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": false,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    processing: true,
                    serverSide: true,
                    iDisplayLength: 25,
                    ajax: {
                        url: '{{url("get-governing-body")}}',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    },
                    columns: [
                        {data: 'sl_no'},
                        {data: 'name', name: 'name'},
                        {data: 'designation', name: 'designation'},
                        {data: 'duration', name: 'duration'},
                        {data: 'phone', name: 'phone'},
                        {data: 'action', name: 'action', orderable: true, searchable: true}
                    ],
                    "aaSorting": []
                });

                t.on('order.dt search.dt', function () {
                    t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                    });
                }).draw();
            });
        });

$(document).ready(function(){
            $(document).on('click', '#profile', function(e){

                e.preventDefault();
                var url = $(this).data('url');
                $.ajax({
                    url: url,
                    type: 'POST',
                    dataType: 'json',
                    headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        },
                    success: function (data) {

                        console.log(data);
                        var location = '';

                        var name = data.name;
                        name = name.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                            return letter.toUpperCase();
                        });

                         $(".modal-body .name").html(name);
                         $(".title").html(name);
                         if (data.designation != null) {
                            $(".modal-body .designation").html(data.designation);
                         }
                         if (data.qualification != null) {
                            $(".modal-body .qualification").html(data.qualification);
                         }

                         if (data.email != null) {
                            $(".modal-body .email").html(data.email);
                         }
                         if (data.phone != null) {
                            $(".modal-body .phone").html(data.phone);
                         }

                        if(data.photo != null) {
                            var img = "uploads/" + data.photo;
                            $(".modal-body .profile-img").attr('src', "{{asset('')}}" + img);
                        }

                        if(data.gender == 1) {
                            $(".modal-body .gender").html('Male');
                        }else if(data.gender == 2) {
                            $(".modal-body .gender").html('Female');
                        }else {
                            $(".modal-body .gender").html('Other');
                        }
                        if (data.address != null) {
                            $(".modal-body .address").html(data.address);
                         }
                         if (data.join_date != null) {
                            $(".modal-body .join_date").html(data.join_date);
                         }
                        if (data.message != null) {
                            $(".modal-body .message").html(data.message);
                         }

                    }
                });

            });

        });
    </script>

@stop