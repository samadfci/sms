@extends('frontEnd.layouts')
@section('pages')
        <div class="post-title">{{ ucfirst($about->title) }}</div> 
            <div class="welcome-post bg-div">
              {!! $about->description !!}
            </div>

            <div class="top-padding clearfix"></div>
            <div class="row">
              <div class="col-md-12">
                <div class="top-notice">
                  <div class="top-notice-title">Notice Board</div>
                  <div class="top-notice-line">
                    <ul>
                      @foreach($notice as $value)
                      <li><a href="{{ url('single-notice-open/'. Crypt::encrypt($value->id)) }}" target="_blank"><i class="fa fa-check"></i>  {{ $value->title }}</a></li>
                      @endforeach
                    </ul>
                    <div class="show-button"><a href="{{ url('view-notice') }}" target="_blank">View all</a></div>
                  </div>
                </div>
              </div>
            </div>
           <div class="top-padding clearfix"></div>
            <!-- notic and event -->
            
            <div class="event">
                 <div class="row">
                   <div class="col-md-12">
                    <div class="event-div">
                    <div class="top-notice-title">Events</div>
                    @foreach($events as $event )

                   <div class="col-sm-4 event-pad">
                              <!--Card-->
                      <div class="event-con">
                        <div class="card">
                          <!--Card image-->
                          @if(!empty($event->photo))
                          <img class="img-responsive" src="{{ url('uploads/'.$event->photo) }}">
                          @else
                          <img class="img-responsive" style="height: 175px;" src="{{ url('uploads/default.png') }}">                      
                          @endif
                          <!--Card content-->
                          <div class="card-body">
                              <!--Title-->
                              <h4 class="card-title">{{ substr($event->title, 0, 18) }} ...</h4>
                              <!--Text-->
                              <p class="card-text">
                                <i>{{ date('Y-M-d', strtotime($event->publish_date)) }}</i>
                              </p>
                              <p class="card-text" style="line-height: 20px;font-size: 16px;">{{ strip_tags(substr($event->details, 0, 100)) }} ...</p>
                              
                              <div class="text-center">
                                <a href="{{ url('single-event-open/'. Crypt::encrypt($event->id)) }}" target="_blank">Details</a>
                              </div>
                          </div>
                      </div>
                      </div>
                      <!--/.Card-->
                   </div>

                   @endforeach
                   </div>
                   </div>
                 </div>
                 <div class="show-button" style="padding-bottom: 10px;"><a href="{{ url('view-event') }}" target="_blank">View all</a></div>
            </div>
            <!-- end notics and event -->
       <div class="top-padding clearfix"></div>

       <div class="discover-div">
                <div class="row" id="counter">
                  <div class="col-md-12">
                   <div class="col-md-4 pad-buttun">
                     <div class="discover">
                        <div class="fa-div">
                        <i class="fa fa-graduation-cap"></i>
                        </div>
                        <div class="text-center toppad-sm">
                           <!-- <span class="total-number">140</span><br><br> -->
                           <span class="counter-value total-number" data-count="{{ (!is_null($setting->total_teacher) ? $setting->total_teacher : 0) }}">0</span><br><br>
                           <span class="discover-title toppad-sm">Total Teachers</span>
                        </div>
                     </div>
                   </div> 

                    <div class="col-md-4 pad-buttun">
                      <div class="discover">
                         <div class="fa-div">
                          <i class="fa fa-book"></i>
                        </div>
                          <div class="text-center toppad-sm">
                          <span class="counter-value total-number" data-count="{{ (!is_null($setting->total_teacher) ? $setting->total_student : 0) }}">0</span><br><br>
                          <span class="discover-title toppad-sm">Total Students</span>
                        </div>
                      </div>
                    </div> 

                    <div class="col-md-4 pad-buttun">
                      <div class="discover">
                         <div class="fa-div">
                          <i class="fa fa-users"></i>
                        </div>
                        <div class="text-center toppad-sm">
                          <span class="counter-value total-number" data-count="{{ (!is_null($setting->total_teacher) ? $setting->total_committee : 0) }}">0</span><br><br>
                          <span class="discover-title toppad-sm">total Committe</span>
                        </div>
                      </div>
                    </div>  
                  </div>
                </div>
            </div>

            <div class="top-padding clearfix"></div>
      <!-- start photos gallery -->
             <!-- <div class="row"> -->
               <div class="col-md-12 gallery-div">
                <div class="gallery-title">Photo Gallary</div>
                 <!-- <div class="gallery-div"> -->
                   <div id="owl-demo" class="owl-carousel owl-theme">
                    @foreach($gallery as $value)
                  <div class="col-md-3 img-gallery">
                    <div class="item"><img src="{{ url('uploads/'.$value->photo) }}" alt="{{ $value->title }}" class="img-responsive"></div>
                  </div>
                  @endforeach
               </div>
                 <!-- </div> -->
                 <div class="show-button" style="padding-bottom: 10px;"><a href="{{ url('/view-gallary') }}" target="_blank">View all</a></div>
             </div>

             <div class="top-padding clearfix"></div>
             
@stop
