
@extends('frontEnd.layouts')
@section('style')
<style type="text/css">
	.welcome-page-title{
		text-align: center;
		padding-top: 15px;
		font-size: 24px;
		margin-bottom: 20px;
		line-height: 1.5;
		color: #000;
		text-transform: uppercase;
		position: relative;
		font-family: "Roboto Slab", serif;
	    font-weight: 400;
	    margin: 0 0 20px 0;
	}
	.single-notice{
		background: #f5f5f5 none repeat scroll 0 0;
	    border : 1px solid #cccccc !important;
	}
</style>
@stop
@section('pages')
	<div class="page-title"><i class="fa fa-home"></i> Notice Board</div><br>
<div class="welcome-post single-notice">
  <div class="welcome-page-title">{{ $notice->title }}</div>
  @if(!is_null($notice->notices))	
  {!! $notice->notices !!}
  @endif
</div> <br>
    <div class="panel panel-default">
        <div class="panel-body">
            @if(!is_null($notice->pdf))
                <h4 style="text-align: left;"></h4>
                <?php
                $fileUrl = public_path() . '/uploads/' . $notice->pdf;

                if(file_exists($fileUrl)) {
                ?>
                <object style="display: block; margin: 0 auto;" width="100%" height="900"
                        type="application/pdf"
                        data="/uploads/<?php echo  $notice->pdf ?>#toolbar=1&amp;navpanes=0&amp;scrollbar=1&amp;page=1&amp;view=FitH"></object>
                <?php } else { ?>
                <div class="">No such file is existed!</div>
                <?php } ?>

            @endif
        </div>
    </div>        
@stop
