@extends('frontEnd.layouts')
@section('style')
   <style type="text/css">
   	      .routine{
   	      	background: #f5f5f5 none repeat scroll 0 0;
			border : 1px solid #cccccc !important;
   	      }
   	      #notices tr > th{
   	      	background-color: #683091;
   	      	color: #fff;
   	      	font-size: 16px;
   	      }
   </style>
@stop
@section('pages')
<div class="page-title"><i class="fa fa-home"></i> Exam Routine</div><br>
<div class="welcome-post routine">
      <div class="table-responsive">
        <table id="notices" class="table table-striped resultTable display table-bordered" role="grid">
        <thead>
        <tr>
            <th>#</th>
            <th>Class</th>
            <th>Routine Type</th>
            <th>Session</th>
            <th>publish date</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        	
        </tbody>
       </table>
      </div>
 </div>     
@stop

@section('js')
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>"/>
    <script language="javascript">
        $(document).ready(function () {
            $(function () {
                var t = $('#notices').DataTable({
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": false,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    processing: true,
                    serverSide: true,
                    iDisplayLength: 10,
                    ajax: {
                        url: '{{url("exam-routine")}}',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    },
                    columns: [
                        {data: 'sl_no'},
                        {data: 'class_name'},
                        {data: 'type'},
                        {data: 'year',name:'year'},
                        {data: 'publish_date',name:'publish_date'},
                        {data: 'action', name: 'action', orderable: true, searchable: true}
                    ],
                    "aaSorting": []
                });

                t.on('order.dt search.dt', function () {
                    t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                    });
                }).draw();
            });
        });
    </script>

@stop