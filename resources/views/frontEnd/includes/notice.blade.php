<div class="top-padding"></div>
        <div class="notice sticky">
        <div class="container-fluid">
          <div class="row">
            <div class="col-lg-2 col-md-3 col-sm-2 col-xs-12 padding-0">
              <div class="flash-box hidden-xs">
                <p class="align-middle">Update Notice</p>
              </div>
            </div>
            <div class="col-lg-10 col-md-9 col-sm-10 col-xs-12">
              <marquee scrollamount="5" onmouseover="this.stop();" onmouseout="this.start();">
                <ul class="list-inline">
                    @foreach($notice as $value)
                      <li><a href="#">{{ $value->title }}</a></li>
                    @endforeach
                 </ul>
              </marquee>
            </div>
          </div>
        </div>
     </div>
<div class="top-padding"></div>