       <div class="row">
          <div class="col-md-12">
            <div id="ft" class="footer hidden-xs" style="background: url('frontEnd/image/footer_top_bg.png') repeat scroll 0 0 rgba(0, 0, 0, 0);">
            </div>
          </div>
         <div class="clearfix"></div>
          <div class="col-md-12">
             <div class="footer-con">
               <div class="col-md-4 col-sm-4" style="padding-left: 60px">
                  <h3 class="footer-title"> Contact Us</h3>
                  <table class="address">
                    <tr>
                      <td><i class="fa fa-map-marker font-color"></i></td>
                      <td style="padding-left: 10px;">{{ (!is_null($contact->address) ? $contact->address : '') }}</td>
                    </tr>

                    <tr>
                      <td><i class="fa fa-phone font-color"></i></td>
                      <td style="padding-left: 10px;">{{ (!is_null($contact->phone) ? $contact->phone : '') }}</td>
                    </tr>
                    <tr>
                      <td><i class="fa fa-envelope font-color"></i></td>
                      <td style="padding-left: 10px;">{{ (!is_null($contact->email) ? $contact->email : '') }}</td>
                    </tr>
                  </table>
               </div>


               <div class="col-md-4 col-sm-4 usefull_link" style="padding-left: 60px">
              <h3 class="footer-title"> USEFUL LINKS</h3>
                  <ul class="list-unstyled">
                      <li><a href="{{ url('/') }}"><i class="fa fa-angle-double-right font-color"></i> Home</a></li>
                      <li><a href="{{ url('class-routine') }}"><i class="fa fa-angle-double-right font-color"></i> Class Routine</a></li>
                      <li><a href="{{ url('exam-routine') }}"><i class="fa fa-angle-double-right font-color"></i> Exam Routine</a></li>
                      <li><a href="{{ url('view-notice') }}"> <i class="fa fa-angle-double-right font-color"></i> Notices</a></li>
                      <li><a href="{{ url('view-event') }}"><i class="fa fa-angle-double-right font-color"></i> Events</a></li>
                     
                  </ul>
              </div>
              <div class="col-md-4 col-sm-4 social-icon" style="padding-left: 60px">
                  <h3 class="footer-title">Social media</h3>
                <ul class="list-inline">
                <li><a href="{{ URL::to($setting->facebook) }}" target="_blank"><i class="fa fa-facebook-f fb-color"></i></a></li>
                <li><a href="{{ URL::to($setting->google) }}" target="_blank"><i class="fa fa-google-plus g-color"></i></a></li>
                <li><a href="{{ URL::to($setting->youtube) }}" target="_blank"><i class="fa fa-youtube y-color"></i></a></li>
              </ul>
              
             </div>
             <div class="clearfix"></div>
             </div>     
        </div>
        <div class="col-md-12">
          <div class="end">
               <span class="pull-right" style="padding-right: 20px">&copy; {{ $setting->copy_right }}</span>
               <span class="pull-left" style="padding-left: 20px">Developed by : <a href="https://www.facebook.com/daudfci" target="_blank"> Daudul islam</a></span>
               <div class="clearfix"></div>
           </div>
        </div>
      </div>
        <!-- end container --> 