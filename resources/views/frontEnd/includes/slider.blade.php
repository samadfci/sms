      <!-- <div class="col-md-12 line"></div> -->
      <div class="row">
          <div class="col-md-12">
            <div class="slider" >
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    <?php for ($i=0; $i < count($slider); $i++) { ?> 
                      <li data-target="#myCarousel" data-slide-to="<?php echo $i; ?>" class="{{ ($i == 0) ? 'active' : '' }}"></li>
                     <?php } ?>
                  </ol>

                  <!-- Wrapper for slides -->
                  <div class="carousel-inner">
                    <?php $j = 0 ?>
                    @foreach($slider as $value)
                    <div class="item {{ (in_array($j++, ['0']) ? 'active' : '') }}">
                      <img src="{{ url('uploads/'.$value->photo) }}" alt="{{(!is_null($value->title)) ? $value->title : ''}}" style="width:100%;">
                    </div>
                    @endforeach
                  </div>

                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                  </a>
                </div>
          </div>
          </div>
        </div>