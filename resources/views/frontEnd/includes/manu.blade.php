<div class="row">
          <div class="col-md-12">
               <!-- Static navbar -->
              <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                      <span class="sr-only">Toggle navigation</span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="{{ url('/') }}"><i class="fa fa-home" aria-hidden="true" style="font-size: 25px"></i></a>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">
                      <li><a href="{{ url('about-us') }}">About Us</a></li>

                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Administration <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="{{ url('academic-calendar') }}"> Academic Calendar</a></li>
                          <li><a href="{{ url('governing-body') }}"> Governing Body</a></li>
                          <li><a href="{{ url('ex-governing-body') }}"> Ex-governing Body</a></li>
                          <li><a href="{{ url('view-chairman-message') }}">Chairman Message</a></li> 
                        </ul>
                      </li>

                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Teachers <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="{{ url('teachers-staff') }}">Teachers/ Staff</a></li>
                          <li><a href="{{ url('ex-teachers-staff') }}">Ex-teachers/ Staff</a></li>
                          <li><a href="{{ url('view-principal-message') }}">Principal Message</a></li>
                        </ul>
                      </li>

                       <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Routines <span class="caret"></span></a>
                        <ul class="dropdown-menu">
                          <li><a href="{{ url('class-routine') }}">Class Routine</a></li>
                          <li><a href="{{ url('exam-routine') }}">Exam Routine</a></li>
                        </ul>
                      </li>

                      

                      <li><a href="{{ url('exam-result') }}">Results</a></li>
                      <li><a href="{{ url('view-gallary') }}">Gallary</a></li>
                      <li><a href="{{ url('view-notice') }}">Notices</a></li>
                      <li><a href="{{ url('view-event') }}">Events</a></li>
                      <li><a href="{{ url('contact') }}">Contact</a></li>
                      <li><a href="{{ url('login') }}" target="_blank">Login</a></li>

                    </ul>
                  </div>
                 </div>
             </nav>
          </div>
        </div>