@extends('frontEnd.home')
@section('content')
@if(Request::is('/'))
@include('frontEnd.includes.slider')                   
@endif
<div style="padding: 2px;background-color: #683091"></div>
@include('frontEnd.includes.manu')
@if(Request::is('/'))
@include('frontEnd.includes.notice')
@else
<br>                   
@endif
 

      <div class="row">
          <div class="col-md-9">
            @yield('pages')
  <!-- end right side -->
          </div>
          <div class="col-md-3">
          <div class="row">
            <div class="col-md-12">
              <div class="post-title-middle">Chairman Message</div>
              @if(!empty($chairman))
              <div class="message-box">
                <div class="message-img">
                  @if(!empty($chairman->photo))
                  <img src="{{ url('uploads/'.$chairman->photo) }}" class="img-responsive">
                  @else
                  <img src="{{ url('uploads/profile.jpg') }}" class="img-responsive">
                  @endif
                </div>

                <div class="card-body">
                    <!--Title-->
                    <h4 class="card-title">{{ ucwords($chairman->chairman_name) }}</h4>
                    <!--Text-->
                    <h4 class="card-text text-center">{{ ucwords($chairman->designation) }}</h4>
                    <div class="show-button">
                      <a href="{{ url('view-chairman-message') }}">View Message</a>
                    </div>
                </div>

              </div>
              @else
                <p class="text-center text-danger">Please update chairman information !</p>  
              @endif
            </div>
          </div>
          <div class="top-padding clearfix"></div>

          <div class="row">
            <div class="col-md-12">
              <div class="post-title-middle">Principal Message</div>
               @if(!empty($principal))
              <div class="message-box">
                <div class="message-img">
                  @if(!empty($principal->photo))
                  <img src="{{ url('uploads/'.$principal->photo) }}" class="img-responsive">
                  @else
                  <img src="{{ url('uploads/profile.jpg') }}" class="img-responsive">
                  @endif
                </div>

                <div class="card-body">
                    <!--Title-->
                    <h4 class="card-title">{{ ucwords($principal->chairman_name) }}</h4>
                    <!--Text-->
                    <h4 class="card-text text-center">{{ ucwords($principal->designation) }}</h4>
                    <div class="show-button">
                      <a href="{{ url('view-principal-message') }}">View Message</a>
                    </div>
                </div>

              </div>
              @else
                <p class="text-center text-danger">Please update principal information !</p>  
              @endif
            </div>
          </div>
         <div class="top-padding clearfix"></div>

          <div class="row">
            <div class="col-md-12">
              <div class="post-title-middle">Important Links</div>
              <div class="important-link">
                <ul class="list-unstyled">
                  @foreach($importantLink as $link)
                  <li><a href="{{ $link->link }}" target="_blank"><i class="fa fa-angle-double-right font-color"></i> {{ ucwords($link->title) }}</a></li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
          <div class="top-padding clearfix"></div>

           <div class="row">
            <div class="col-md-12">
              <div class="post-title-middle">Calendar</div>
              <div class="calendar">
                <div id="my-calendar"></div>
              </div>
            </div>
          </div>
          <div class="top-padding clearfix"></div>


              <!-- end left side -->
          </div>
        </div> 
         <div class="top-padding clearfix"></div>
         <div class="top-padding clearfix"></div>
@include('frontEnd.includes.footer')
         

@stop