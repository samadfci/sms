  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <link rel="shortcut icon" href="image/logo.jpg" />
      <link rel="stylesheet" type="text/css" href="{{ asset('frontEnd/css/style.css') }}">
      
      <!-- Bootstrap Core CSS -->
      <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

      <!--testimonial css-->
      <!-- <link rel="stylesheet" type="text/css" href="css/overview.css"> -->

      <!--owl carousel css-->
      <link rel="stylesheet" type="text/css" href="{{ asset('frontEnd/owlCarousel/css/owl.carousel.min.css') }}">
      <link rel="stylesheet" type="text/css" href="{{ asset('frontEnd/owlCarousel/css/owl.theme.default.min.css') }}">

      <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">

      <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.9/css/all.css" integrity="sha384-5SOiIsAziJl6AWe0HWRKTXlfcSHKmYV4RBF18PPJ173Kzn7jzMyFuTtk8JA7QQG1" crossorigin="anonymous"> -->
      <!-- <link rel="stylesheet" href="https://maxcdn.icons8.com/fonts/line-awesome/1.1/css/line-awesome-font-awesome.min.css"> -->
      <!-- <link rel="stylesheet" type="text/css" href="lightbox/ekko-lightbox.css"> -->
      
      <!-- jsCalendar -->
    <link rel="stylesheet" type="text/css" href="{{ asset('frontEnd/calender/jsCalendar.css') }}">

    <!-- dataTable --> 
    <link rel="stylesheet" href="{{ asset("assets/datatable/dataTables.bootstrap.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/datatable/responsive.bootstrap.min.css") }}" />


<!-- <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css"> -->
  


    @yield('style')
    </head>
    <body>
      <!-- <a id="button"></a>
      <div class="col-md-12">
        <div class="lang">
        <a href="{{ url('/bn') }}" target="_blank">বাংলা</a>
        <a href="{{ url('/en') }}">English</a>
      </div>
      </div> -->
     <div class="container">
     <div class="banner">
          <div class="row">
              <div class="col-md-12">
                   @if(!empty($setting->logo))
                   <img src="{{ URL::to('/uploads/'.$setting->logo) }}" height="80px" >
                   @endif
                  <span>Hichachora High School</span><br>
              </div>
          </div>
      </div>

      
      @yield('content')
    </div>

      <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script> -->
      <script src="{{ asset('frontEnd/js/jquery.js') }}"></script>
       <script src="{{ asset('frontEnd/js/bootstrap.min.js') }}"></script>
      <!-- Include all compiled plugins (below), or include individual files as needed -->
      <script src="{{ asset('frontEnd/owlCarousel/js/owl.carousel.min.js') }}"></script>
      <script type="text/javascript">
       $(document).ready(function() {
          $("#owl-demo").owlCarousel({ 
          items : 4,
          autoplay:true,
          autoplayTimeout:2000,
          autoplayHoverPause:true,
          touchDrag : true,  
        });
        });
     </script>
     
       <!-- image light box -->
     <!--  <script src="lightbox/ekko-lightbox.js"></script>
      <script src="lightbox/ekko-lightbox.js.map"></script>
      <script src="lightbox/ekko-lightbox.min.js"></script>
      <script src="lightbox/ekko-lightbox.min.js.map"></script> -->

      <script type="text/javascript">

        //$(document).on('click', '[data-toggle="lightbox"]', function(event) {
          //          event.preventDefault();
          //          $(this).ekkoLightbox();
          //      });
      </script>

<!-- jsCalendar -->
    <script type="text/javascript" src="{{ asset('frontEnd/calender/jsCalendar.js') }}"></script>

    <!-- Create the calendar -->
    <script type="text/javascript">
      // Create the calendar
      var calendar = jsCalendar.new("#my-calendar");
      var a = 0;
      $(window).scroll(function() {

        var oTop = $('#counter').offset().top - window.innerHeight;
        if (a == 0 && $(window).scrollTop() > oTop) {
          $('.counter-value').each(function() {
            var $this = $(this),
              countTo = $this.attr('data-count');
            $({
              countNum: $this.text()
            }).animate({
                countNum: countTo
              },

              {

                duration: 7000,
                easing: 'swing',
                step: function() {
                  $this.text(Math.floor(this.countNum));
                },
                complete: function() {
                  $this.text(this.countNum);
                  //alert('finished');
                }

              });
          });
          a = 1;
        }

      });
      $('ul.nav li.dropdown').hover(function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(0.5).fadeIn(500);
      }, function() {
      $(this).find('.dropdown-menu').stop(true, true).delay(0.5).fadeOut(500);
      });

        var btn = $('#button');

        $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
          btn.addClass('show');
        } else {
          btn.removeClass('show');
        }
        });

        btn.on('click', function(e) {
        e.preventDefault();
        $('html, body').animate({scrollTop:0}, '300');
        });

        $(window).scroll(function() {
        if ($(window).scrollTop() > 300) {
          $('.lang').addClass('hide');
        } else {
          $('.lang').removeClass('hide');
        }
        });

    </script>
    <!-- dataTable js -->
    <script src="{{ asset("assets/datatable/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("assets/datatable/dataTables.bootstrap.min.js") }}"></script>
    <script src="{{ asset("assets/datatable/dataTables.responsive.min.js") }}"></script>
    <script src="{{ asset("assets/datatable/responsive.bootstrap.min.js") }}"></script>
    <!-- <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script> -->



    @yield('js')
       
    </body>
  </html>