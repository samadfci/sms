@extends('app.layouts')
@section('style')
<style type="text/css">
	.tox-notification {display: none !important;}
</style>
@stop
@section('content')
   <div class="panel panel-primary">
      <div class="panel-heading"><i class="fa fa-edit"></i> Setting Edit</div>
      <div class="panel-body">

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif

        @if(session()->has('success'))
           <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <strong>Success! </strong> {{ session('success') }}
          </div>
         @endif

         @if(session()->has('error'))
                  <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong>Warning! </strong> {{ session('error') }}
                  </div>
          @endif

    {!! Form::open(['url' => 'setting/setting-update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}

    <div class="form-group">
      <label class="control-label col-sm-2" for="description">Logo:</label>
      <div class="col-sm-10">          
        <input type="file" class="form-control-file" id="photo" name="photo">
        <span class="text-danger" style="font-size: 9px; font-weight: bold">[File Format: *.jpeg,.jpg,.png,.gif | Maximum Photo size 128kb]</span>
        <br>
        @if($setting->logo)
           <img src="{{URL::to('/uploads/'.$setting->logo)}}" id="previousImage" alt="Preview Image" width="80px" height="80px" />
        @endif
        <img src="" class="hidden" id="imagePreview" alt="Preview Image" width="80px" height="80px" />
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="total_teacher">Total Teachers:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="total_teacher" placeholder="Enter Total Teachers" name="total_teacher" value="{{ $setting->total_teacher }}">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2" for="total_student">Total Students:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="total_student" placeholder="Enter Total Students" name="total_student" value="{{ $setting->total_student }}">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="total_committee">Total Committees:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="total_committee" placeholder="Enter Total Committees" name="total_committee" value="{{ $setting->total_committee }}">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="facebook">Facebook Link:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="facebook" placeholder="Enter Facebook Link" name="facebook" value="{{ $setting->facebook }}">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="youtube">Youtube Link:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="youtube" placeholder="Enter Youtube Link" name="youtube" value="{{ $setting->youtube }}">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="google">Google+ Link:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="google" placeholder="Enter Google+ Link" name="google" value="{{ $setting->google }}">
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="copy_right">Copy Right:</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="copy_right" placeholder="Enter Copy Right" name="copy_right" value="{{ $setting->copy_right }}">
      </div>
    </div>

    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Update</button>
      </div>
    </div>
  {!! Form::close() !!}
</div>
@stop

@section('js')
<script src='https://cloud.tinymce.com/5/tinymce.min.js?apiKey=your_API_key'></script>
<script type="text/javascript">
	tinymce.init({
  selector: 'textarea',  // change this value according to your HTML
  height: 300,
    plugins: [
       'autosave advlist autolink lists link image charmap print preview hr anchor pagebreak',
       'searchreplace wordcount visualblocks visualchars code fullscreen',
       'insertdatetime media nonbreaking save table contextmenu directionality',
       'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
      toolbar2: 'forecolor backcolor emoticons',
    content_css: [
       '//www.tinymce.com/css/codepen.min.css'
    ]
});

	$('#photo').change(function(){			
			readImgUrlAndPreview(this);
			function readImgUrlAndPreview(input){
				 if (input.files && input.files[0]) {
			            var reader = new FileReader();
			            reader.onload = function (e) {			            
                      $('#imagePreview').removeClass("hidden"); 
			                $('#previousImage').addClass("hidden");	
			                $('#imagePreview').attr('src', e.target.result);
							}
			          };
			          reader.readAsDataURL(input.files[0]);
			     }	
		});


</script>

@stop