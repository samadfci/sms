  @extends('app.layouts')
  @section('style')
  <style type="text/css">
    .tox-notification {display: none !important;}
  </style>
  @stop
  @section('content')
     <div class="panel panel-primary">
        <div class="panel-heading">
                  <div class="pull-left">
                      <strong><i class="fa fa-edit"></i> Edit Result Type</strong>
                  </div>
                  <div class="clearfix"></div>
          </div>
        <div class="panel-body">

          @if ($errors->any())
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if(session()->has('success'))
             <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Success! </strong> {{ session('success') }}
            </div>
           @endif

           @if(session()->has('error'))
                    <div class="alert alert-warning alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <strong>Warning! </strong> {{ session('error') }}
                    </div>
            @endif

            {!! Form::open(['url' => 'setting/result-type/update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
            <input type="hidden" name="id" value="{{ $result_type->id }}">
            <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="name">Class Name:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="name" placeholder="Enter Result Type" name="name" value="{{ $result_type->name }}">
              </div>
            </div>
            
           <div class="form-group">
              <label class="control-label col-sm-2" for="pdf">Status :</label>
                  <div class="col-sm-10">
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="1" {{ $result_type->status == 1 ? 'checked' : '' }}>Active
                    </label>
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="0" {{ $result_type->status == 0 ? 'checked' : '' }}>Inactive
                    </label>
                  </div>
            </div>
            
        </div>
       <div class="panel-footer">
         <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <a href="{{ url('setting/result-type') }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Cencle</a>
              <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"></i> Update</button>
            </div>
          </div>

          <div class="clearfix"></div>
       </div>
        {!! Form::close() !!}    
      </div>

  @stop
