@extends('app.layouts')
@section('style')
<style type="text/css">
	.tox-notification {display: none !important;}
  .panel-heading{padding: 5px 15px !important;}
</style>
@stop
@section('content')
   <div class="panel panel-primary">
      <div class="panel-heading">
        <div class="pull-left">
            <h5><strong><i class="fa fa-list"></i> All Class</strong></h5>
        </div>
        <div class="pull-right">
                <a class="btn btn-sm btn-default" href="{{ url('setting/class-form') }}">
                    <i class="fa fa-plus"></i><strong> Add Class</strong>
                </a>
        </div>
        <div class="clearfix"></div>
    </div>
      <div class="panel-body">
          @if(session()->has('success'))
             <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Success! </strong> {{ session('success') }}
            </div>
           @endif

          <div class="table-responsive">
            <table id="designation" class="table table-striped resultTable display table-bordered" role="grid">
            <thead>
            <tr>
                <th>#</th>
                <th>Class Name</th>
                <th>Status</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
           </table>
          </div>
      </div>
    </div>
@stop

@section('js')
<input type="text" name="_token" value="<?php echo csrf_token(); ?>"/>
    <script language="javascript">
        $(document).ready(function () {
            $(function () {
                var t = $('#designation').DataTable({
                    "columnDefs": [{
                        "searchable": true,
                        "orderable": false,
                        "targets": 0
                    }],
                    "order": [[0, 'asc']],
                    processing: true,
                    serverSide: true,
                    iDisplayLength: 25,
                    ajax: {
                        url: '{{url("get-class-list")}}',
                        method: 'POST',
                        headers: {
                            'X-CSRF-TOKEN': '{{ csrf_token() }}'
                        }
                    },
                    columns: [
                        {data: 'sl_no'},
                        {data: 'name'},
                        {data: 'status', name: 'status'},
                        {data: 'action', name: 'action', orderable: true, searchable: true}
                    ],
                    "aaSorting": []
                });

                t.on('order.dt search.dt', function () {
                    t.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                        cell.innerHTML = i + 1;
                    });
                }).draw();
            });
        });
    </script>
@stop
