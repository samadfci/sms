  @extends('app.layouts')
  @section('style')
  <style type="text/css">
    .tox-notification {display: none !important;}
  </style>
  @stop
  @section('content')
     <div class="panel panel-primary">
        <div class="panel-heading">
                  <div class="pull-left">
                      <strong><i class="fa fa-align-justify"></i> Contact</strong>
                  </div>
                  <div class="clearfix"></div>
          </div>
        <div class="panel-body">

          @if ($errors->any())
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if(session()->has('success'))
             <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Success! </strong> {{ session('success') }}
            </div>
           @endif

           @if(session()->has('error'))
                    <div class="alert alert-warning alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <strong>Warning! </strong> {{ session('error') }}
                    </div>
            @endif

            {!! Form::open(['url' => '/setting/contact-update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}

            <div class="form-group">
              <label class="control-label col-sm-2" for="title">Address:</label>
              <div class="col-sm-10">
                <input type="text" name="address" class="form-control" value="{{ $contact->address }}">
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="title">Phone No:</label>
              <div class="col-sm-10">
                <input type="text" name="phone" class="form-control" value="{{ $contact->phone }}">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="title">Email:</label>
              <div class="col-sm-10">
                <input type="email" name="email" class="form-control" value="{{ $contact->email }}">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="title">Map:</label>
              <div class="col-sm-10">
                <input type="text" name="map" class="form-control" value="{{ $contact->map }}">
              </div>
            </div>


        </div>
    
       <div class="panel-footer">
         <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"></i> Update</button>
            </div>
          </div>

          <div class="clearfix"></div>
       </div>
        {!! Form::close() !!}    
      </div>

  @stop

