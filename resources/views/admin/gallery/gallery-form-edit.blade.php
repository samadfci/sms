  @extends('app.layouts')
  @section('style')
  <style type="text/css">
    .tox-notification {display: none !important;}
  </style>
  @stop
  @section('content')
     <div class="panel panel-primary">
        <div class="panel-heading">
                  <div class="pull-left">
                      <strong><i class="fa fa-edit"></i> Edit Gallery Iamge</strong>
                  </div>
                  <div class="clearfix"></div>
          </div>
        <div class="panel-body">

          @if ($errors->any())
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if(session()->has('success'))
             <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Success! </strong> {{ session('success') }}
            </div>
           @endif

           @if(session()->has('error'))
                    <div class="alert alert-warning alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <strong>Warning! </strong> {{ session('error') }}
                    </div>
            @endif

            {!! Form::open(['url' => 'gallery/gallery-update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
            <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="title">Title :</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="title" placeholder="Enter Title" name="title" value="{{ $gallery->title }}">
              </div>
            </div>
            <input type="hidden" name="id" value="{{ $gallery->id }}">
            <div class="form-group">
              <label class="control-label col-sm-2" for="description">Photo:</label>
              <div class="col-sm-10">          
                <input type="file" class="form-control-file" id="photo" name="photo">
                <span class="text-danger" style="font-size: 9px; font-weight: bold">[File Format: *.jpeg,.jpg,.png,.gif | Maximum Photo size 3MB]</span>
                <br>
                @if($gallery->photo)
                   <img src="{{URL::to('/uploads/'.$gallery->photo)}}" id="previousImage" alt="Preview Image" width="100%" height="300px" />
                @endif
                <img src="" class="hidden" id="imagePreview" alt="Preview Image" width="100%" height="300px" />
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pdf">Status :</label>
                  <div class="col-sm-10">
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="1" {{ $gallery->status == 1 ? 'checked' : '' }}>Publish
                    </label>
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="0" {{ $gallery->status == 0 ? 'checked' : '' }}>Unpublish
                    </label>
                  </div>
            </div>

            

        </div>
        
       <div class="panel-footer">
         <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <a href="{{ url('gallery/gallery-list') }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Cencle</a>
              <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"></i> Save</button>
            </div>
          </div>

          <div class="clearfix"></div>
       </div>
        {!! Form::close() !!}    
      </div>
  @stop

  @section('js')
    <script type="text/javascript">
      $('#photo').change(function(){      
      readImgUrlAndPreview(this);
      function readImgUrlAndPreview(input){
         if (input.files && input.files[0]) {
                  var reader = new FileReader();
                  reader.onload = function (e) {                  
                      $('#imagePreview').removeClass("hidden"); 
                      $('#previousImage').addClass("hidden"); 
                      $('#imagePreview').attr('src', e.target.result);
              }
                };
                reader.readAsDataURL(input.files[0]);
           }  
    });

    </script>
  @stop
