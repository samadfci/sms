  @extends('app.layouts')
  @section('style')
  <style type="text/css">
    .tox-notification {display: none !important;}
  </style>
  @stop
  @section('content')
     <div class="panel panel-primary">
        <div class="panel-heading">
                  <div class="pull-left">
                      <strong><i class="fa fa-align-justify"></i> 
                        @if(Request::segment(2) == 'edit')
                        Edit Notice
                        @else
                        View Notice
                        @endif

                      </strong>
                  </div>
                  <div class="clearfix"></div>
          </div>
        <div class="panel-body">

          @if ($errors->any())
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if(session()->has('success'))
             <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Success! </strong> {{ session('success') }}
            </div>
           @endif

           @if(session()->has('error'))
                    <div class="alert alert-warning alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <strong>Warning! </strong> {{ session('error') }}
                    </div>
            @endif

            {!! Form::open(['url' => '/notices/notices-update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
            <input type="hidden" name="id" value="{{ $notice->id }}">
            <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="title">Title:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="title" placeholder="Enter title" name="title" value="{{ $notice->title }}">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="description">Notice:</label>
              <div class="col-sm-10">          
                <textarea id="mytextarea" class="form-control" placeholder="You can write Notices here..." name="notices">{{ $notice->notices }}</textarea>
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pdf">Notice in Pdf:</label>
              <div class="col-sm-10">          
             <input name="pdf"
                    id="noticeFile" type="file"
                    size="20"
                    onchange="uploadDocument('preview_80', this.id, 'pdf', '0')"/>
             <span class="text-danger" id="alert_10"
                   style="color: green; font-size: 9px; font-weight: bold;display:block; ">[File Format: *.pdf | File size within 3 MB]</span>

              <div class="save_file">
               <?php if (!empty($notice->pdf)) { ?>
               <a target="_blank" class="btn btn-xs btn-primary show-in-view" title=""
                  href="{{URL::to('/uploads/' .$notice->pdf)}}"><i
                           class="fa fa-file-pdf-o"></i> Open File
               </a>
               <?php } ?>
              </div>
             <div id="preview_80">
                 <input type="hidden" value="<?php
                      if ($notice->pdf != 0) {
                          echo $notice->pdf;
                      }
                      ?>" id="pdf" name="pdf"/>
             </div>
             </div>
              </div>
           

            <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="pdf">publish Date:</label>
                  <div class="col-sm-10">
                    <div class='datepicker input-group date' data-date-format="dd-mm-yyyy">
                      <input type='text' value="{{ (($notice->publish_date != '0000-00-00') ? date('d-M-Y', strtotime($notice->publish_date)):'') }}" class="form-control" name="publish_date" placeholder="dd-mm-yyyy" />
                      <div class="input-group-addon">
                         <i class="fa fa-calendar">
                         </i>
                      </div>
                  </div>
                  </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="pdf">Status :</label>
                  <div class="col-sm-10">
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="1" {{ $notice->status == 1 ? 'checked' : '' }}>publish
                    </label>
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="0" {{ $notice->status == 2 ? 'checked' : '' }}>Unpublish
                    </label>
                  </div>
            </div>
        </div>
    
       <div class="panel-footer">
         <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <a href="{{ url('/notices-list') }}" class="btn btn-info"><i class="fa fa-times" aria-hidden="true"></i> Cencle</a>
              @if($viewMode == 'on')
              <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"></i> Update</button>
              @endif
            </div>
          </div>

          <div class="clearfix"></div>
       </div>
        {!! Form::close() !!}    
      </div>

  @stop

  @section('js')
  <script src='https://cloud.tinymce.com/5/tinymce.min.js?apiKey=your_API_key'></script>
  <script type="text/javascript">
      tinymce.init({
      selector: 'textarea',
      height: 300,
      plugins: [
         'autosave advlist autolink lists link image charmap print preview hr anchor pagebreak',
         'searchreplace wordcount visualblocks visualchars code fullscreen',
         'insertdatetime media nonbreaking save table contextmenu directionality',
         'emoticons template paste textcolor colorpicker textpattern imagetools'
      ],
      toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
        toolbar2: 'forecolor backcolor emoticons',
      content_css: [
         '//www.tinymce.com/css/codepen.min.css'
      ]
      });
      </script>
      
      <script type="text/javascript">
      //datePicker ....
          var today = new Date();
          var yyyy = today.getFullYear();

          $('.datepicker').datetimepicker({
              viewMode: 'years',
              format: 'DD-MMM-YYYY',
              minDate: '01/01/'+(yyyy-100),
              maxDate: '01/01/'+(yyyy+100)
          });
         
          var $preview = $(".preview");
          $preview.hide();

          $("input").on("change", function(){

          var files = this.files;
          var fileReader = new FileReader();

          fileReader.onload = function(e){
            $preview.attr("href", e.target.result);
            $preview.show();
          };

          fileReader.readAsDataURL(files[0]);
          });

          function uploadDocument(targets, id, vField, isRequired) {
          var inputFile = $("#" + id).val();
          // var ext = inputFile.split('.').pop();
         // document.getElementById("isRequired").value = isRequired;
         // document.getElementById("selected_file").value = id;
         // document.getElementById("validateFieldName").value = vField;
         document.getElementById(targets).style.color = "red";
         var action = "{{url('notices/upload-document')}}";
         $("#" + targets).html('Uploading....');
         var file_data = $("#" + id).prop('files')[0];
         var form_data = new FormData();
         form_data.append('selected_file', id);
         form_data.append('isRequired', isRequired);
         form_data.append('validateFieldName', vField);
         form_data.append('_token', "{{ csrf_token() }}");
         form_data.append(id, file_data);
         $.ajax({
             target: '#' + targets,
             url: action,
             dataType: 'text', // what to expect back from the PHP script, if anything
             cache: false,
             contentType: false,
             processData: false,
             data: form_data,
             type: 'post',
             success: function (response) {
              
                 $('#' + targets).html(response);
                 var fileNameArr = inputFile.split("\\");
                 var l = fileNameArr.length;
                 if ($('#label_' + id).length)
                     $('#label_' + id).remove();
                 var newInput = $('<label id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                 $("#" + id).after(newInput);
                 console.log(id);

                 //====New Code===//
                 //var span = document.getElementById('text-danger');
                 //$(span).after(newInput);

                 //check valid data
                 var validate_field = $('#' + vField).val();
                 if (validate_field == '') {
                     document.getElementById(id).value = '';
                 }
             }
         });    
  }
     
  </script>
  @if($viewMode == 'off')
  <script type="text/javascript">
    $(document).ready(function() { 
       $('input[type="text"]').each(function(){
         $(this).attr('readonly','readonly');
       });  
       $("#noticeFile").prop('disabled', true);
       $("input[type=radio]").attr('disabled', true);
       tinymce.activeEditor.setMode('readonly'); 
     });
  </script>
  @endif
  @stop

