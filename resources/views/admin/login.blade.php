@extends('app.home')
@section('style')

@stop
@section('body')
 <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="schoolName">
                    <span>হিছাছরা বহুমুখী উচ্ছ বিদ্যালয়</span>
                </div>
            </div>
        
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-green">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                        @if(session()->has('successMsg'))
                           <div class="alert alert-info alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <strong>Success!</strong>{{ session('successMsg') }}
                            </div>
                        @endif

                       {!! Form::open(['url' => 'login','method'=>'post']) !!}
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}" autofocus>
                                    @if($errors->has('email'))
                                        <span class="text-danger">
                                            {{ $errors->first('email') }}
                                        </span>
                                    @endif

                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="{{ old('password') }}">

                                   @if($errors->has('password'))
                                        <span class="text-danger">
                                            {{ $errors->first('password') }}
                                        </span>
                                    @endif
                                </div>

                                @if(session()->has('errorMsg'))
                                   <div class="alert alert-warning alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                        <strong>Warning!</strong>   {{ session('errorMsg') }}
                                    </div>
                                @endif
                            
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-info btn-block">Login</button>
                            </fieldset>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop