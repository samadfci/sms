@extends('app.layouts')
@section('style')
<style type="text/css">
  .tox-notification {display: none !important;}
</style>
@stop
@section('content')
   <div class="panel panel-primary">
      <div class="panel-heading">
          <div class="pull-left">
              <strong><i class="fa fa-align-justify"></i> 
                 @if(Request::segment(2) == 'edit')
                  Edit Teacher & Stuff
                  @else
                  Edit Teacher & Stuff
                  @endif
              </strong>
          </div>
          <div class="clearfix"></div>
        </div>
      <div class="panel-body">

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif

        @if(session()->has('success'))
           <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <strong>Success! </strong> {{ session('success') }}
          </div>
         @endif

         @if(session()->has('error'))
                  <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong>Warning! </strong> {{ session('error') }}
                  </div>
          @endif

          {!! Form::open(['url' => 'teacher-update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
          <div class="form-group">
            <label class="control-label col-sm-2 required-star" for="name">Name :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="Name" placeholder="Enter Name" name="name" value="{{ $teacher->name }}">
            </div>
          </div>
         <input type="hidden" name="id" value="{{ $teacher->id }}">
          <div class="form-group">
            <label class="control-label col-sm-2 required-star" for="designation_id">Designation :</label>
            <div class="col-sm-10">
              <select class="form-control" name="designation_id">
                <option value="0">Select Designation</option>
                @foreach($designation as $value)
                <option value="{{ $value->id }}" @if($value->id == $teacher->designation_id) selected="selected" @endif >{{ $value->name }}</option>
                @endforeach
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="teacher_id">Teacher Id :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="teacher_id" placeholder="Enter Teacher Id" name="teacher_id" value="{{ $teacher->teacher_id }}">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="qualification">Qualification :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="qualification" placeholder="Enter Qualification" name="qualification" value="{{ $teacher->qualification }}">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="email">Email :</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" value="{{ $teacher->email }}">
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2 required-star" for="phone">Phone no :</label>
            <div class="col-sm-10">
              <input type="text" class="form-control" id="phone" placeholder="01810 000000" name="phone" value="{{ $teacher->phone }}">
            </div>
          </div>

          <div class="form-group">
          <label class="control-label col-sm-2" for="gender">Gender :</label>
              <div class="col-sm-10">
                <label class="radio-inline">
                  <input type="radio" id="gender" name="gender" value="1" checked="checked" {{ $teacher->gender == 1 ? 'checked' : '' }}>Male
                </label>
                <label class="radio-inline">
                  <input type="radio" id="gender" name="gender" value="2" {{ $teacher->gender == 2 ? 'checked' : '' }}>Female
                </label>
              </div>
        </div>
        <div class="form-group">
            <label class="control-label col-sm-2" for="message">Address :</label>
            <div class="col-sm-10">          
              <textarea id="mytextarea" rows="2x3" class="form-control" placeholder="You can write Address here..." name="address">{{ ucwords($teacher->address) }}</textarea>
            </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2 required-star" for="join_date">Join Date:</label>
                <div class="col-sm-10">
                  <div class='datepicker input-group date' data-date-format="dd-mm-yyyy">
                    <input type='text' class="form-control" name="join_date" placeholder="dd-mm-yyyy" value="{{ (($teacher->join_date != '0000-00-00') ? date('d-M-Y', strtotime($teacher->join_date)):'') }}" />
                    <div class="input-group-addon">
                       <i class="fa fa-calendar">
                       </i>
                    </div>
                </div>
                </div>
          </div>

          <div class="form-group">
            <label class="control-label col-sm-2" for="message">Message:</label>
            <div class="col-sm-10">          
              <textarea id="mytextarea" rows="5x3" class="form-control" placeholder="You can write messages here..." name="message">{{ $teacher->message }}</textarea>
            </div>
          </div>

          <div class="form-group">
          <label class="control-label col-sm-2" for="description">Photo:</label>
          <div class="col-sm-10">          
            <input type="file" class="form-control-file" id="photo" name="photo">
            <span id="alertMessage" class="text-danger" style="font-size: 9px; font-weight: bold">[File Format: *.jpeg,.jpg,.png,.gif | Maximum Photo size 3MB]</span>
            <br>
            @if($teacher->photo)
           <img src="{{URL::to('/uploads/'.$teacher->photo)}}" id="previousImage" alt="Preview Image" width="180px" height="180px" />
            @endif
            <img src="" class="hidden" id="imagePreview" alt="Preview Image" width="180px" height="180px" />
          </div>
        </div>

        <div class="form-group">
          <label class="control-label col-sm-2" for="status">Status :</label>
              <div class="col-sm-10">
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" onchange="addRejoinedDate()" value="1" {{ $teacher->status == 1 ? 'checked' : '' }}>Active
                </label>
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" onchange="addRejoinedDate()" value="2" {{ $teacher->status == 2 ? 'checked' : '' }}>Inactive
                </label>
              </div>
        </div>

            <div class="form-group hidden" id="showInputBox">
            <label class="control-label col-sm-2" for="rejoined_date">Rejoined Date:</label>
                <div class="col-sm-10">
                  <div class='datepicker input-group date' data-date-format="dd-mm-yyyy">
                    <input type='text' class="form-control" name="rejoined_date" placeholder="dd-mm-yyyy" value="{{ ((!empty($teacher->rejoined_date)) ? date('d-M-Y', strtotime($teacher->rejoined_date)):'') }}" />
                    <div class="input-group-addon">
                       <i class="fa fa-calendar">
                       </i>
                    </div>
                </div>
                </div>
          </div>

          <!-- end panel body -->
      </div>
     <div class="panel-footer">
       <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <a href="{{ url('/teacher-list') }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Cencle</a>
            @if($viewMode == 'on')
            <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"></i> Update</button>
            @endif
          </div>
        </div>

        <div class="clearfix"></div>
     </div>
      {!! Form::close() !!}    
    </div>

@stop

@section('js')

<script type="text/javascript">
  //datePicker ....
        var today = new Date();
        var yyyy = today.getFullYear();

        $('.datepicker').datetimepicker({
            viewMode: 'years',
            format: 'DD-MMM-YYYY',
            minDate: '01/01/'+(yyyy-100),
            maxDate: '01/01/'+(yyyy+100)
        });

        $('#photo').change(function(){      
          readImgUrlAndPreview(this);
          function readImgUrlAndPreview(input){
           if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {                  
                $('#imagePreview').removeClass("hidden"); 
                $('#previousImage').addClass("hidden"); 
                $('#imagePreview').attr('src', e.target.result);
                }
              };
              reader.readAsDataURL(input.files[0]);
             }  
        });


        function addRejoinedDate(){
           if ($("input[name='status']:checked").val() == 2) {
              $('#showInputBox').removeClass('hidden');  
           }else{
            $('#showInputBox').addClass('hidden'); 
           }
        }
        $(document).ready(function(){
          if ($("input[name='status']:checked").val() == 2) {
              $('#showInputBox').removeClass('hidden');  
           }else{
            $('#showInputBox').addClass('hidden'); 
           }
        })
</script>

@if($viewMode == 'off')
  <script type="text/javascript">
    $(document).ready(function() { 
       $('input[type="text"]').each(function(){
         $(this).attr('readonly','readonly');
       });
       $("textarea").attr('readonly', 'readonly');
       $('input[type="email"]').each(function(){
         $(this).attr('readonly','readonly');
       });  
       $('option:not(:selected)').attr('disabled', true);
       $("input[type=radio]").attr('disabled', true);
       document.getElementById("photo").style.display = "none"; 
       document.getElementById("alertMessage").style.display = "none"; 
     });
  </script>
  @endif


@stop
