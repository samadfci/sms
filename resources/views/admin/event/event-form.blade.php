@extends('app.layouts')
@section('style')
<style type="text/css">
	.tox-notification {display: none !important;}
</style>
@stop
@section('content')
   <div class="panel panel-primary">
      <div class="panel-heading">
        <div class="pull-left">
            <strong><i class="fa fa-align-justify"></i> Create Event</strong>
        </div>
        <div class="clearfix"></div>
      </div>
      <div class="panel-body">

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif

        @if(session()->has('success'))
           <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <strong>Success! </strong> {{ session('success') }}
          </div>
         @endif

         @if(session()->has('error'))
                  <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong>Warning! </strong> {{ session('error') }}
                  </div>
          @endif

    {!! Form::open(['url' => 'event/event-store','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
    
    <div class="form-group">
      <label class="control-label col-sm-2 required-star" for="title">Title :</label>
      <div class="col-sm-10">
        <input type="text" class="form-control" id="title" placeholder="Enter title" name="title" value="{{ old('title') }}">
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-sm-2 required-star" for="details">Details :</label>
      <div class="col-sm-10">          
        <textarea id="mytextarea" class="form-control" placeholder="You can write details here..." name="details"></textarea>
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2" for="description">Photo :</label>
      <div class="col-sm-10">          
        <input type="file" class="form-control-file" id="photo" name="photo">
        <span class="text-danger" style="font-size: 9px; font-weight: bold">[File Format: *.jpeg,.jpg,.png,.gif | Maximum Photo size 3MB]</span>
        <br>
        <img src="" class="hidden" id="imagePreview" alt="Preview Image" width="100%" height="300px" />
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-sm-2 required-star" for="pdf">Publish Date :</label>
          <div class="col-sm-10">
            <div class='datepicker input-group date' data-date-format="dd-mm-yyyy">
              <input type='text' class="form-control" name="publish_date" placeholder="dd-mm-yyyy" />
              <div class="input-group-addon">
                 <i class="fa fa-calendar">
                 </i>
              </div>
          </div>
          </div>
    </div>

    <div class="form-group">
    <label class="control-label col-sm-2" for="pdf">Status :</label>
        <div class="col-sm-10">
          <label class="radio-inline">
            <input type="radio" id="status" name="status" value="1" checked>Publish
          </label>
          <label class="radio-inline">
            <input type="radio" id="status" name="status" value="0">Unpublish
          </label>
        </div>
  </div>

</div>
<div class="panel-footer">
     <div class="form-group">        
        <div class="col-sm-offset-2 col-sm-10">
          <a href="{{ url('event/event-list') }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Cencle</a>
          <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"></i> Save</button>
        </div>
      </div>

      <div class="clearfix"></div>
   </div>
  {!! Form::close() !!}

@stop

@section('js')
<script src='https://cloud.tinymce.com/5/tinymce.min.js?apiKey=your_API_key'></script>
<script type="text/javascript">
	tinymce.init({
  selector: 'textarea',  // change this value according to your HTML
  height: 300,
    plugins: [
       'autosave advlist autolink lists link image charmap print preview hr anchor pagebreak',
       'searchreplace wordcount visualblocks visualchars code fullscreen',
       'insertdatetime media nonbreaking save table contextmenu directionality',
       'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
      toolbar2: 'forecolor backcolor emoticons',
    content_css: [
       '//www.tinymce.com/css/codepen.min.css'
    ]
});

	$('#photo').change(function(){			
			readImgUrlAndPreview(this);
			function readImgUrlAndPreview(input){
				 if (input.files && input.files[0]) {
			            var reader = new FileReader();
			            reader.onload = function (e) {			            
                      $('#imagePreview').removeClass("hidden"); 
			                $('#previousImage').addClass("hidden");	
			                $('#imagePreview').attr('src', e.target.result);
							}
			          };
			          reader.readAsDataURL(input.files[0]);
			     }	
		});

  //datePicker ....
          var today = new Date();
          var yyyy = today.getFullYear();

          $('.datepicker').datetimepicker({
              viewMode: 'years',
              format: 'DD-MMM-YYYY',
              minDate: '01/01/'+(yyyy-100),
              maxDate: '01/01/'+(yyyy+100)
          });


</script>

@stop