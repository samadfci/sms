@extends('app.layouts')
@section('style')
<style type="text/css">
	.tox-notification {display: none !important;}
</style>
@stop
@section('content')
   <div class="panel panel-primary">
      <div class="panel-heading">Chairman Message Body</div>
      <div class="panel-body">

        @if ($errors->any())
          <div class="alert alert-danger">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
          </div>
        @endif

        @if(session()->has('success'))
           <div class="alert alert-success alert-dismissable">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
              <strong>Success! </strong> {{ session('success') }}
          </div>
         @endif

         @if(session()->has('error'))
                  <div class="alert alert-warning alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <strong>Warning! </strong> {{ session('error') }}
                  </div>
          @endif

    {!! Form::open(['url' => 'chairman-message-update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
    <input type="hidden" name="message_type" value="{{ empty($message->designation_id) ? '' : $message->designation_id }}">
    <input type="hidden" name="id" value="{{ empty($message->id) ? '' : $message->id }}">
    <div class="form-group">
      <label class="control-label col-sm-2" for="description">Message Body:</label>
      <div class="col-sm-10">          
        <textarea id="mytextarea" class="form-control" placeholder="You can write message here..." name="message_body">{!! empty($message->message_body) ? '' : $message->message_body !!}</textarea>
      </div>
    </div>

       <div class="form-group">
          <label class="control-label col-sm-2" for="status">Status :</label>
              <div class="col-sm-10">
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" value="1" {{ !empty($message->status) == 1 ? 'checked' : '' }}>Publish
                </label>
                <label class="radio-inline">
                  <input type="radio" id="status" name="status" value="0" {{ !empty($message->status) == 0 ? 'checked' : '' }}>Unpublish
                </label>
              </div>
        </div>


        <div class="form-group">        
          <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-success">Update</button>
          </div>
        </div>
  {!! Form::close() !!}
</div>
@stop

@section('js')
<script src='https://cloud.tinymce.com/5/tinymce.min.js?apiKey=your_API_key'></script>
<script type="text/javascript">
	tinymce.init({
  selector: 'textarea',  // change this value according to your HTML
  height: 500,
    plugins: [
       'autosave advlist autolink lists link image charmap print preview hr anchor pagebreak',
       'searchreplace wordcount visualblocks visualchars code fullscreen',
       'insertdatetime media nonbreaking save table contextmenu directionality',
       'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent',
      toolbar2: 'forecolor backcolor emoticons',
    content_css: [
       '//www.tinymce.com/css/codepen.min.css'
    ]
});


</script>

@stop