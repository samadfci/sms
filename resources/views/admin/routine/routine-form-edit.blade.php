  @extends('app.layouts')
  @section('style')
  <style type="text/css">
    .tox-notification {display: none !important;}
  </style>
  @stop
  @section('content')
     <div class="panel panel-primary">
        <div class="panel-heading">
                  <div class="pull-left">
                      <strong><i class="fa fa-align-justify"></i> Create Class Routine</strong>
                  </div>
                  <div class="clearfix"></div>
          </div>
        <div class="panel-body">

          @if ($errors->any())
            <div class="alert alert-danger">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          @if(session()->has('success'))
             <div class="alert alert-success alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                <strong>Success! </strong> {{ session('success') }}
            </div>
           @endif

           @if(session()->has('error'))
                    <div class="alert alert-warning alert-dismissable">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                      <strong>Warning! </strong> {{ session('error') }}
                    </div>
            @endif

            {!! Form::open(['url' => '/routine/routine-update','method'=>'post','class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}

            <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="title">Class:</label>
              <div class="col-sm-10">
                <input type="text" id="className" name="" value="{{ $routine->name }}" class="form-control">
              </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2" for="title">Title:</label>
              <div class="col-sm-10">
                <input type="text" class="form-control" id="title" placeholder="Enter title" name="title" value="{{ $routine->title }}">
              </div>
            </div>

          
            <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="pdf">Routine in Pdf:</label>
              <div class="col-sm-10">          
             <input name="pdf" id="8" type="file" size="20" onchange="uploadDocument('preview_80', this.id, 'pdf', '0')"/>
              <span class="text-danger" id="alert_10"
                   style="color: green; font-size: 9px; font-weight: bold;display:block; ">
                 [File Format: *.pdf | File size within 3 MB]
              </span>

               <div class="save_file">
               <?php if (!empty($routine->pdf_file)) { ?>
               <a target="_blank" class="btn btn-xs btn-primary show-in-view" title=""
                  href="{{URL::to('/uploads/' .$routine->pdf_file)}}"><i
                           class="fa fa-file-pdf-o"></i> Open File
               </a>
               <?php } ?>
              </div>
            
             <div id="preview_80">
                 <input type="hidden" id="pdf" name="pdf"/>
             </div>
             </div>
              </div>
           <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="pdf">Session:</label>
                  <div class="col-sm-10">
                    <div class='yearpicker input-group date' data-date-format="yyyy">
                      <input type='text' class="form-control" name="year" placeholder="yyyy" value="{{ $routine->year }}" />
                      <div class="input-group-addon">
                         <i class="fa fa-calendar">
                         </i>
                      </div>
                  </div>
                  </div>
            </div>

            <div class="form-group">
              <label class="control-label col-sm-2 required-star" for="pdf">publish Date:</label>
                  <div class="col-sm-10">
                    <div class='datepicker input-group date' data-date-format="dd-mm-yyyy">
                      <input type='text' class="form-control" name="publish_date" placeholder="dd-mm-yyyy" value="{{ (!is_null($routine->publish_date))? date('d-M-Y', strtotime($routine->publish_date)):'' }}" />
                      <div class="input-group-addon">
                         <i class="fa fa-calendar">
                         </i>
                      </div>
                  </div>
                  </div>
            </div>
            <div class="form-group">
              <label class="control-label col-sm-2" for="pdf">Status :</label>
                  <div class="col-sm-10">
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="1" {{ $routine->status == 1 ? 'checked' : '' }}>publish
                    </label>
                    <label class="radio-inline">
                      <input type="radio" id="status" name="status" value="0" {{ $routine->status == 0 ? 'checked' : '' }}>Unpublish
                    </label>
                  </div>
            </div>

        </div>
       <input type="hidden" name="id" value="{{ $routine->id }}">
       <div class="panel-footer">
         <div class="form-group">        
            <div class="col-sm-offset-2 col-sm-10">
              <a href="{{ url('routine/routine-list') }}" class="btn btn-danger"><i class="fa fa-times" aria-hidden="true"></i> Cencle</a>
              <button type="submit" class="btn btn-md btn-success"><i class="fa fa-save"></i> Update</button>
            </div>
          </div>

          <div class="clearfix"></div>
       </div>
        {!! Form::close() !!}    
      </div>

  @stop

  @section('js')
    <script type="text/javascript">
          //datePicker ....
          var today = new Date();
          var yyyy = today.getFullYear();

          $('.datepicker').datetimepicker({
              viewMode: 'years',
              format: 'DD-MMM-YYYY',
              minDate: '01/01/'+(yyyy-100),
              maxDate: '01/01/'+(yyyy+100)
          });
          $('.yearpicker').datetimepicker({
              viewMode: 'years',
              format: 'YYYY',
              extraFormats: [ 'DD.MM.YY', 'DD.MM.YYYY' ],
            // maxDate: 'now',
             minDate: '01/01/1905'
          });
         
          var $preview = $(".preview");
          $preview.hide();

          $("input").on("change", function(){

          var files = this.files;
          var fileReader = new FileReader();

          fileReader.onload = function(e){
            $preview.attr("href", e.target.result);
            $preview.show();
          };

          fileReader.readAsDataURL(files[0]);
          });

          function uploadDocument(targets, id, vField, isRequired) {
          var inputFile = $("#" + id).val();
          // var ext = inputFile.split('.').pop();
         // document.getElementById("isRequired").value = isRequired;
         // document.getElementById("selected_file").value = id;
         // document.getElementById("validateFieldName").value = vField;
         document.getElementById(targets).style.color = "red";
         var action = "{{url('notices/upload-document')}}";
         $("#" + targets).html('Uploading....');
         var file_data = $("#" + id).prop('files')[0];
         var form_data = new FormData();
         form_data.append('selected_file', id);
         form_data.append('isRequired', isRequired);
         form_data.append('validateFieldName', vField);
         form_data.append('_token', "{{ csrf_token() }}");
         form_data.append(id, file_data);
         $.ajax({
             target: '#' + targets,
             url: action,
             dataType: 'text', // what to expect back from the PHP script, if anything
             cache: false,
             contentType: false,
             processData: false,
             data: form_data,
             type: 'post',
             success: function (response) {
              
                 $('#' + targets).html(response);
                 var fileNameArr = inputFile.split("\\");
                 var l = fileNameArr.length;
                 if ($('#label_' + id).length)
                     $('#label_' + id).remove();
                 var newInput = $('<label id="label_' + id + '"><br/><b>File: ' + fileNameArr[l - 1] + '</b></label>');
                 $("#" + id).after(newInput);
                 console.log(id);

                 //====New Code===//
                 //var span = document.getElementById('text-danger');
                 //$(span).after(newInput);

                 //check valid data
                 var validate_field = $('#' + vField).val();
                 if (validate_field == '') {
                     document.getElementById(id).value = '';
                 }
             }
         });
    
  }

  
    $(document).ready(function() { 
       $('#className').attr('readonly','readonly');
     });    
  </script>
  @stop

