@extends('app.home')
@section('body')
 <!-- Navigation -->
        @include('navigation.nav')
        <div id="page-wrapper">
          <br>
            @yield('content')
        </div>
        <!-- /#page-wrapper -->
@stop
