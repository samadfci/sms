<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>HS-Admin</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('assets/vendor/metisMenu/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('assets/dist/css/sb-admin-2.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('assets/vendor/morrisjs/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('assets/vendor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
     <link href="https://fonts.googleapis.com/css?family=Orbitron" rel="stylesheet" type="text/css"/>

    <!-- custom css -->
    <link href="{{ asset('assets/style.css') }}" rel="stylesheet" type="text/css">
    
     <!-- dataTable --> 
    <link rel="stylesheet" href="{{ asset("assets/datatable/dataTables.bootstrap.min.css") }}" />
    <link rel="stylesheet" href="{{ asset("assets/datatable/responsive.bootstrap.min.css") }}" />
    <!-- datePicker css -->
    <link rel="stylesheet" href="{{ asset("assets/datepicker/bootstrap-datetimepicker.css") }}" />
    <style type="text/css">
        .nav>li>a{
            font-size: 16px !important;
        }
        .sidebar{
            margin-top: 53px !important;
        } 
    </style>
   
    @yield('style')

</head>

<body>
    
    @yield('body')
    

  <!-- jQuery -->
    <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('assets/vendor/metisMenu/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('assets/vendor/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/morrisjs/morris.min.js') }}"></script>
    <script src="{{ asset('assets/data/morris-data.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/dist/js/sb-admin-2.js') }}"></script>

    <script type="text/javascript">
            
    function getTime( ) {
    var d = new Date( ); 
    d.setHours( d.getHours() + 2 ); // offset from local time
    var h = (d.getHours() % 12) || 12; // show midnight & noon as 12
    return (
        ( h < 10 ? '0' : '') + h +
        ( d.getMinutes() < 10 ? ':0' : ':') + d.getMinutes() +
                // optional seconds display
        ( d.getSeconds() < 10 ? ':0' : ':') + d.getSeconds() + 
        ( d.getHours() >= 12 ? ' AM' : ' PM' )
    );
    
    }

    var clock = document.getElementById('clock');
    setInterval( function() { clock.innerHTML = getTime(); }, 1000 );
    </script>

    <!-- dataTable js -->
    <script src="{{ asset("assets/datatable/jquery.dataTables.min.js") }}"></script>
    <script src="{{ asset("assets/datatable/dataTables.bootstrap.min.js") }}"></script>
    <script src="{{ asset("assets/datatable/dataTables.responsive.min.js") }}"></script>
    <script src="{{ asset("assets/datatable/responsive.bootstrap.min.js") }}"></script>
    
    <!-- datepicker -->
    <script src="{{ asset("assets/datepicker/moment.js") }}"></script>
    <script src="{{ asset("assets/datepicker/bootstrap-datetimepicker.js") }}"></script>

    @yield('js')

</body>

</html>
