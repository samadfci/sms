         <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="{{ url('/dashboard') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="{{ url('about/about-us') }}"><i class="fa fa-check-square"></i> Abouts Us</a>
                           
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="{{ url('/notices-list') }}"><i class="fa fa-check-square"></i> Notices board</a>
                           
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-check-square"></i> Governing Body<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('designation-list') }}"> Designation</a>
                                </li>
                                <li>
                                    <a href="{{ url('committee-list') }}"> Committee</a>
                                </li>
                                <li>
                                    <a href="{{ url('ex-committee-list') }}"> Ex-committee</a>
                                </li>
                                <li>
                                    <a href="{{ url('chairman-message') }}"> Chairman Message</a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-check-square"></i> Teachers & Stuff<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('teacher-designation-list') }}"> Designations</a>
                                </li>
                                <li>
                                    <a href="{{ url('teacher-list') }}"> Teachers & Stuff</a>
                                </li>
                                <li>
                                    <a href="{{ url('ex-teacher-list') }}"> Ex-teachers & Stuff</a>
                                </li>
                                <li>
                                    <a href="{{ url('principal-message') }}"> Principal Message</a>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="{{ url('link/link-list') }}"><i class="fa fa-check-square"></i> Important Link</a>
                        </li>
                        <li>
                            <a href="{{ url('event/event-list') }}"><i class="fa fa-check-square"></i> Event</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ url('gallery/gallery-list') }}"><i class="fa fa-check-square"></i> Gallery</a>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="{{ url('routine/routine-list') }}"><i class="fa fa-check-square"></i> Class Routine</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ url('exam/exam-routine-list') }}"><i class="fa fa-check-square"></i> Exam Routine</a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{ url('result/result-list') }}"><i class="fa fa-check-square"></i> Result</a>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="{{ url('calendar/calendar-list') }}"><i class="fa fa-check-square"></i> Academin Calendar</a>
                            <!-- /.nav-second-level -->
                        </li>

                        <li>
                            <a href="#"><i class="fa fa-cog" aria-hidden="true"></i> Settings<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{ url('setting/class') }}"> Class</a>
                                </li>
                                <li>
                                    <a href="{{ url('setting/result-type') }}"> Result Type</a>
                                </li>
                                <li>
                                    <a href="{{ url('setting/slider/slider-list') }}"> Slider Image</a>
                                </li>
                                <li>
                                    <a href="{{ url('setting/Contact-view') }}"> Contact</a>
                                </li>
                                <li>
                                    <a href="{{ url('setting/setting-view') }}"> Settings</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>